function out = model
%
% trident.m
%
% Model exported on Mar 19 2024, 12:16 by COMSOL 6.0.0.405.

import com.comsol.model.*
import com.comsol.model.util.*

model = ModelUtil.create('Model');

model.hist.disable;

model.modelPath('/home/amarkowi/xinfer/xinfer');

model.label('trident.mph');

model.param.set('x0', '0', 'origin x');
model.param.set('y0', '0', 'origin y');
model.param.set('z0', '0', 'origin z');
model.param.set('h_left', '9.9286572[mm]', 'height of leftmost trident');
model.param.set('h_middle', '19.9616454[mm]', 'height of middle trident');
model.param.set('h_right', '16.14342677[mm]', 'height of rightmost trident');
model.param.set('h_leg', '0.51988291[mm]', 'height of trident leg');
model.param.set('h_box', '4.30834107[mm]', 'height of trident box');
model.param.set('fin_space_l', '1.92676586[mm]', 'space between left and middle trident fins');
model.param.set('fin_space_r', '0.77742054[mm]', 'space between right and middle trident fins');
model.param.set('w_left', '0.94480912[mm]', 'width of outer trident fins');
model.param.set('w_mid', '1.34457567[mm]', 'width of middle trident fin');
model.param.set('w_right', '0.53541852[mm]', 'width of right trident fin');
model.param.set('h_coat', '24.3[mm]', 'starting y coord of coating above the base');
model.param.set('thickness', '100[um]', 'substrate thickness');
model.param.set('h_clamp', '2.7[mm]', 'clamp height along base (for meshing, mostly)');
model.param.set('h_tot', '28[mm]', 'total structure height');
model.param.set('w_leg', '1[mm]', 'width of trident leg');
model.param.set('h_base', 'h_tot - max(max(h_left, h_right), h_middle) - h_box - h_leg', 'height of base piece');
model.param.set('w_box', 'fin_space_l + fin_space_r + w_left + w_mid + w_right', 'width of trident box');
model.param.set('w_base', '10[mm]', 'was 19[mm]');
model.param.set('t_block', '50*thickness', 'thickness of clamping block');
model.param.set('coating', '500[nm]', 'coating thickness');
model.param.set('scale_Ge_E', '1', 'scale applied to Ge Young''s modulus');
model.param.set('scale_Ge_nu', '1', 'scale applied to Ge Poisson ratio');
model.param.set('edge_space', 'w_base/2 - w_leg/2', 'distance of edge of leg from edge of base');
model.param.set('w_max', '16[mm]', 'maximum box width');
model.param.set('h_excess_base', 'h_base - h_clamp - 0.5[mm]', 'require at least 0.5 mm of overhang of base above clamp (useful for meshing, minimal impact on results)');
model.param.set('h_excess_coat', 'h_tot - h_coat - h_clamp - 1[mm]', 'require an additional 0.5 mm of uncoated material above h_excess_base. Also to avoid excessive meshing requirements, with minimal impact on result');

model.component.create('comp1', true);

model.component('comp1').geom.create('geom1', 3);

model.result.table.create('tbl1', 'Table');
model.result.table.create('tbl2', 'Table');
model.result.table.create('tbl3', 'Table');
model.result.evaluationGroup.create('std1EvgFrq', 'EvaluationGroup');
model.result.evaluationGroup.create('std1mpf1', 'EvaluationGroup');

model.component('comp1').mesh.create('mesh1');

model.component('comp1').geom('geom1').selection.create('csel1', 'CumulativeSelection');
model.component('comp1').geom('geom1').selection('csel1').label('clamp');
model.component('comp1').geom('geom1').create('wp1', 'WorkPlane');
model.component('comp1').geom('geom1').feature('wp1').set('selresult', true);
model.component('comp1').geom('geom1').feature('wp1').set('selresultshow', 'all');
model.component('comp1').geom('geom1').feature('wp1').set('unite', true);
model.component('comp1').geom('geom1').feature('wp1').geom.create('r1', 'Rectangle');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r1').label('base');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r1').set('pos', {'x0' 'y0'});
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r1').set('size', {'w_base' 'h_base'});
model.component('comp1').geom('geom1').feature('wp1').geom.create('r8', 'Rectangle');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r8').label('leg');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r8').set('pos', {'x0 + edge_space' 'y0 + h_base'});
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r8').set('size', {'w_leg' 'h_leg'});
model.component('comp1').geom('geom1').feature('wp1').geom.create('arr1', 'Array');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('arr1').set('type', 'linear');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('arr1').active(false);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('arr1').label('legs');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('arr1').set('linearsize', 3);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('arr1').set('displ', {'mid_space+w_leg' '0'});
model.component('comp1').geom('geom1').feature('wp1').geom.feature('arr1').selection('input').set({'r8'});
model.component('comp1').geom('geom1').feature('wp1').geom.create('r9', 'Rectangle');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r9').label('box');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r9').set('pos', {'x0+edge_space+w_leg/2' 'y0+h_base+h_leg+h_box/2'});
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r9').set('base', 'center');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r9').set('size', {'w_box' 'h_box'});
model.component('comp1').geom('geom1').feature('wp1').geom.create('arr2', 'Array');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('arr2').set('type', 'linear');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('arr2').active(false);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('arr2').label('boxes');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('arr2').set('linearsize', 3);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('arr2').set('displ', {'mid_space+w_leg' '0'});
model.component('comp1').geom('geom1').feature('wp1').geom.feature('arr2').selection('input').set({'r9'});
model.component('comp1').geom('geom1').feature('wp1').geom.create('r2', 'Rectangle');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r2').label('leftfin');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r2').set('pos', {'x0+edge_space+w_leg/2-w_box/2' 'y0+h_base+h_leg+h_box'});
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r2').set('size', {'w_left' 'h_left'});
model.component('comp1').geom('geom1').feature('wp1').geom.create('r10', 'Rectangle');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r10').label('rightfin');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r10').set('pos', {'x0+edge_space+w_leg/2+w_box/2-w_right' 'y0+h_base+h_leg+h_box'});
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r10').set('size', {'w_right' 'h_right'});
model.component('comp1').geom('geom1').feature('wp1').geom.create('copy1', 'Copy');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('copy1').active(false);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('copy1').label('outfin_short_r');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('copy1').set('displx', 'w_box-w_right');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('copy1').selection('input').set({'r2'});
model.component('comp1').geom('geom1').feature('wp1').geom.create('r3', 'Rectangle');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r3').label('midfin');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r3').set('pos', {'x0+edge_space+w_leg/2 - w_box/2 + w_left + fin_space_l + w_mid/2' 'y0+h_base+h_leg+h_box+h_middle/2'});
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r3').set('base', 'center');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r3').set('size', {'w_mid' 'h_middle'});
model.component('comp1').geom('geom1').feature('wp1').geom.create('r4', 'Rectangle');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r4').active(false);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r4').label('outfin_long__l ');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r4').set('pos', {'x0+edge_space+(3/2)*w_leg+mid_space-w_box/2' 'y0+h_base+h_leg+h_box'});
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r4').set('size', {'outfin' 'h_middle'});
model.component('comp1').geom('geom1').feature('wp1').geom.create('copy2', 'Copy');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('copy2').active(false);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('copy2').label('outfin_long_r');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('copy2').set('displx', 'w_box-outfin');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('copy2').selection('input').set({'r4'});
model.component('comp1').geom('geom1').feature('wp1').geom.create('r5', 'Rectangle');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r5').active(false);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r5').label('midfin_long');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r5').set('pos', {'x0+edge_space+(3/2)*w_leg+mid_space' 'y0+h_base+h_leg+h_box+h_middle/2'});
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r5').set('base', 'center');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r5').set('size', {'midfin' 'h_middle'});
model.component('comp1').geom('geom1').feature('wp1').geom.create('r6', 'Rectangle');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r6').active(false);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r6').label('outfin_middle_l');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r6').set('pos', {'x0+edge_space+(5/2)*w_leg+2*mid_space-w_box/2' 'y0+h_base+h_leg+h_box'});
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r6').set('size', {'outfin' 'h_right'});
model.component('comp1').geom('geom1').feature('wp1').geom.create('copy3', 'Copy');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('copy3').active(false);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('copy3').label('outfin_middle_r ');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('copy3').set('displx', 'w_box-outfin');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('copy3').selection('input').set({'r6'});
model.component('comp1').geom('geom1').feature('wp1').geom.create('r7', 'Rectangle');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r7').active(false);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r7').label('midfin_middle');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r7').set('pos', {'x0+edge_space+(5/2)*w_leg+2*mid_space' 'y0+h_base+h_leg+h_box+h_right/2'});
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r7').set('base', 'center');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('r7').set('size', {'midfin' 'h_right'});
model.component('comp1').geom('geom1').feature('wp1').geom.create('cha1', 'Chamfer');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('cha1').active(false);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('cha1').label('corners');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('cha1').set('dist', '.125[mm]');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('cha1').selection('point').set({'r3(1)' 'copy2(1)' 'r5(1)' 'r7(1)' 'r2(1)' 'copy1(1)' 'r4(1)' 'r6(1)' 'copy3(1)' 'r1(1)'}, [3 4 0 0; 3 4 0 0; 3 4 0 0; 3 4 0 0; 3 4 0 0; 3 4 0 0; 3 4 0 0; 3 4 0 0; 3 4 0 0; 1 2 3 4]);
model.component('comp1').geom('geom1').feature('wp1').geom.create('pt1', 'Point');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('pt1').active(false);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('pt1').set('p', [0 0.003]);
model.component('comp1').geom('geom1').feature('wp1').geom.create('pt2', 'Point');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('pt2').active(false);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('pt2').set('p', [0.019 0.003]);
model.component('comp1').geom('geom1').feature('wp1').geom.create('ls1', 'LineSegment');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('ls1').active(false);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('ls1').set('selresult', true);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('ls1').set('selresultshow', 'all');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('ls1').selection('vertex1').set('pt1(1)', 1);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('ls1').selection('vertex2').set('pt2(1)', 1);
model.component('comp1').geom('geom1').feature('wp1').geom.create('pard1', 'PartitionDomains');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('pard1').active(false);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('pard1').set('selresult', true);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('pard1').set('selresultshow', 'all');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('pard1').set('partitionwith', 'extendededges');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('pard1').selection('extendededge').set('ls1(1)', 1);
model.component('comp1').geom('geom1').feature('wp1').geom.create('uni1', 'Union');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('uni1').set('selresult', true);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('uni1').set('selresultshow', 'all');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('uni1').set('color', 'custom');
model.component('comp1').geom('geom1').feature('wp1').geom.feature('uni1').set('customcolor', [0.6196078658103943 0.5843137502670288 0.5882353186607361]);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('uni1').set('intbnd', false);
model.component('comp1').geom('geom1').feature('wp1').geom.feature('uni1').selection('input').set({'r10' 'r8' 'r9' 'r3(1)' 'r2(1)' 'r1(1)'});
model.component('comp1').geom('geom1').create('ext1', 'Extrude');
model.component('comp1').geom('geom1').feature('ext1').set('inputhandling', 'keep');
model.component('comp1').geom('geom1').feature('ext1').setIndex('distance', 'thickness', 0);
model.component('comp1').geom('geom1').feature('ext1').selection('input').set({'wp1'});
model.component('comp1').geom('geom1').create('csol1', 'ConvertToSolid');
model.component('comp1').geom('geom1').feature('csol1').label('Substrate');
model.component('comp1').geom('geom1').feature('csol1').set('selresult', true);
model.component('comp1').geom('geom1').feature('csol1').set('selresultshow', 'all');
model.component('comp1').geom('geom1').feature('csol1').selection('input').set({'ext1'});
model.component('comp1').geom('geom1').create('wp2', 'WorkPlane');
model.component('comp1').geom('geom1').feature('wp2').set('quickplane', 'zx');
model.component('comp1').geom('geom1').feature('wp2').set('quicky', 'y0+h_tot - h_coat');
model.component('comp1').geom('geom1').feature('wp2').set('unite', true);
model.component('comp1').geom('geom1').create('pard1', 'PartitionDomains');
model.component('comp1').geom('geom1').feature('pard1').set('repairtoltype', 'absolute');
model.component('comp1').geom('geom1').feature('pard1').selection('domain').named('csol1');
model.component('comp1').geom('geom1').create('boxsel2', 'BoxSelection');
model.component('comp1').geom('geom1').feature('boxsel2').set('entitydim', 2);
model.component('comp1').geom('geom1').feature('boxsel2').label('destination substrate faces 1');
model.component('comp1').geom('geom1').feature('boxsel2').set('zmin', 'z0 + thickness / 2');
model.component('comp1').geom('geom1').feature('boxsel2').set('condition', 'allvertices');
model.component('comp1').geom('geom1').feature('boxsel2').set('color', '14');
model.component('comp1').geom('geom1').create('boxsel1', 'BoxSelection');
model.component('comp1').geom('geom1').feature('boxsel1').set('entitydim', 2);
model.component('comp1').geom('geom1').feature('boxsel1').label('coated substrate faces');
model.component('comp1').geom('geom1').feature('boxsel1').set('ymin', 'y0 + h_tot - h_coat');
model.component('comp1').geom('geom1').feature('boxsel1').set('zmin', 'z0 + thickness / 2');
model.component('comp1').geom('geom1').feature('boxsel1').set('condition', 'allvertices');
model.component('comp1').geom('geom1').feature('boxsel1').set('color', '10');
model.component('comp1').geom('geom1').create('wp3', 'WorkPlane');
model.component('comp1').geom('geom1').feature('wp3').active(false);
model.component('comp1').geom('geom1').feature('wp3').set('planetype', 'faceparallel');
model.component('comp1').geom('geom1').feature('wp3').set('unite', true);
model.component('comp1').geom('geom1').create('ext2', 'Extrude');
model.component('comp1').geom('geom1').feature('ext2').label('Coating 1 ');
model.component('comp1').geom('geom1').feature('ext2').set('selresult', true);
model.component('comp1').geom('geom1').feature('ext2').set('selresultshow', 'all');
model.component('comp1').geom('geom1').feature('ext2').set('extrudefrom', 'faces');
model.component('comp1').geom('geom1').feature('ext2').set('inputhandling', 'keep');
model.component('comp1').geom('geom1').feature('ext2').setIndex('distance', 'coating', 0);
model.component('comp1').geom('geom1').feature('ext2').set('crossfaces', false);
model.component('comp1').geom('geom1').feature('ext2').selection('inputface').named('boxsel1');
model.component('comp1').geom('geom1').create('boxsel3', 'BoxSelection');
model.component('comp1').geom('geom1').feature('boxsel3').set('entitydim', 2);
model.component('comp1').geom('geom1').feature('boxsel3').label('destination coating faces');
model.component('comp1').geom('geom1').feature('boxsel3').set('zmin', 'z0 + thickness + coating / 2');
model.component('comp1').geom('geom1').feature('boxsel3').set('condition', 'inside');
model.component('comp1').geom('geom1').feature('boxsel3').set('color', '6');
model.component('comp1').geom('geom1').create('ext3', 'Extrude');
model.component('comp1').geom('geom1').feature('ext3').active(false);
model.component('comp1').geom('geom1').feature('ext3').label('Coating 1  1');
model.component('comp1').geom('geom1').feature('ext3').set('selresult', true);
model.component('comp1').geom('geom1').feature('ext3').set('extrudefrom', 'faces');
model.component('comp1').geom('geom1').feature('ext3').setIndex('distance', 'coating', 0);
model.component('comp1').geom('geom1').create('wp4', 'WorkPlane');
model.component('comp1').geom('geom1').feature('wp4').set('selresult', true);
model.component('comp1').geom('geom1').feature('wp4').set('quickplane', 'zx');
model.component('comp1').geom('geom1').feature('wp4').set('quicky', 'y0 + h_clamp');
model.component('comp1').geom('geom1').create('parf1', 'PartitionFaces');
model.component('comp1').geom('geom1').feature('parf1').set('selresult', true);
model.component('comp1').geom('geom1').feature('parf1').set('selresultshow', 'bnd');
model.component('comp1').geom('geom1').feature('parf1').set('partitionwith', 'workplane');
model.component('comp1').geom('geom1').feature('parf1').selection('face').set('pard1(1)', [3 4 8 9]);
model.component('comp1').geom('geom1').create('ext4', 'Extrude');
model.component('comp1').geom('geom1').feature('ext4').set('contributeto', 'csel1');
model.component('comp1').geom('geom1').feature('ext4').set('selresult', true);
model.component('comp1').geom('geom1').feature('ext4').set('extrudefrom', 'faces');
model.component('comp1').geom('geom1').feature('ext4').set('inputhandling', 'keep');
model.component('comp1').geom('geom1').feature('ext4').setIndex('distance', 't_block', 0);
model.component('comp1').geom('geom1').feature('ext4').selection('inputface').set('parf1(1)', 3);
model.component('comp1').geom('geom1').create('ext5', 'Extrude');
model.component('comp1').geom('geom1').feature('ext5').set('contributeto', 'csel1');
model.component('comp1').geom('geom1').feature('ext5').set('selresult', true);
model.component('comp1').geom('geom1').feature('ext5').set('extrudefrom', 'faces');
model.component('comp1').geom('geom1').feature('ext5').set('inputhandling', 'keep');
model.component('comp1').geom('geom1').feature('ext5').setIndex('distance', 't_block', 0);
model.component('comp1').geom('geom1').feature('ext5').selection('inputface').set('parf1(1)', 4);
model.component('comp1').geom('geom1').feature('fin').set('repairtoltype', 'absolute');
model.component('comp1').geom('geom1').feature('fin').set('absrepairtol', 2.8E-8);
model.component('comp1').geom('geom1').run;

model.component('comp1').view('view1').hideObjects.create('hide1');

model.component('comp1').material.create('mat1', 'Common');
model.material.create('mat2', 'Common', '');
model.component('comp1').material.create('mat4', 'Common');
model.component('comp1').material.create('mat6', 'Common');
model.component('comp1').material('mat1').propertyGroup('def').func.create('alpha_solid_1', 'Piecewise');
model.component('comp1').material('mat1').propertyGroup('def').func.create('C_solid_1', 'Piecewise');
model.component('comp1').material('mat1').propertyGroup('def').func.create('HC_solid_1', 'Piecewise');
model.component('comp1').material('mat1').propertyGroup('def').func.create('VP_solid_1', 'Piecewise');
model.component('comp1').material('mat1').propertyGroup('def').func.create('rho_solid_1', 'Piecewise');
model.component('comp1').material('mat1').propertyGroup('def').func.create('TD', 'Piecewise');
model.component('comp1').material('mat1').propertyGroup.create('ThermalExpansion', 'Thermal expansion');
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').func.create('dL_solid_1', 'Piecewise');
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').func.create('CTE', 'Piecewise');
model.component('comp1').material('mat1').propertyGroup.create('Enu', 'Young''s modulus and Poisson''s ratio');
model.component('comp1').material('mat1').propertyGroup('Enu').func.create('E_solid_100_axis_2', 'Piecewise');
model.component('comp1').material('mat1').propertyGroup('Enu').func.create('nu_solid_100_axis_2', 'Piecewise');
model.component('comp1').material('mat1').propertyGroup.create('KG', 'Bulk modulus and shear modulus');
model.component('comp1').material('mat1').propertyGroup('KG').func.create('mu_solid_100_axis_2', 'Piecewise');
model.material('mat2').propertyGroup('def').func.create('alpha_solid_1', 'Piecewise');
model.material('mat2').propertyGroup('def').func.create('C_solid_1', 'Piecewise');
model.material('mat2').propertyGroup('def').func.create('HC_solid_1', 'Piecewise');
model.material('mat2').propertyGroup('def').func.create('VP_solid_1', 'Piecewise');
model.material('mat2').propertyGroup('def').func.create('rho_solid_1', 'Piecewise');
model.material('mat2').propertyGroup('def').func.create('TD', 'Piecewise');
model.material('mat2').propertyGroup.create('ThermalExpansion', 'Thermal expansion');
model.material('mat2').propertyGroup('ThermalExpansion').func.create('dL_solid_1', 'Piecewise');
model.material('mat2').propertyGroup('ThermalExpansion').func.create('CTE', 'Piecewise');
model.material('mat2').propertyGroup.create('Enu', 'Young''s modulus and Poisson''s ratio');
model.material('mat2').propertyGroup('Enu').func.create('E_solid_100_axis_2', 'Piecewise');
model.material('mat2').propertyGroup('Enu').func.create('nu_solid_100_axis_2', 'Piecewise');
model.material('mat2').propertyGroup.create('KG', 'Bulk modulus and shear modulus');
model.material('mat2').propertyGroup('KG').func.create('mu_solid_100_axis_2', 'Piecewise');
model.component('comp1').material('mat4').selection.set([1 3]);
model.component('comp1').material('mat4').info.create('Composition');
model.component('comp1').material('mat4').propertyGroup('def').func.create('alpha', 'Piecewise');
model.component('comp1').material('mat4').propertyGroup('def').func.create('C', 'Piecewise');
model.component('comp1').material('mat4').propertyGroup('def').func.create('rho', 'Piecewise');
model.component('comp1').material('mat4').propertyGroup.create('ThermalExpansion', 'Thermal expansion');
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').func.create('dL', 'Piecewise');
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').func.create('CTE', 'Piecewise');
model.component('comp1').material('mat4').propertyGroup.create('Enu', 'Young''s modulus and Poisson''s ratio');
model.component('comp1').material('mat4').propertyGroup('Enu').func.create('E', 'Piecewise');
model.component('comp1').material('mat4').propertyGroup('Enu').func.create('nu', 'Piecewise');
model.component('comp1').material('mat4').propertyGroup.create('KG', 'Bulk modulus and shear modulus');
model.component('comp1').material('mat4').propertyGroup('KG').func.create('mu', 'Piecewise');
model.component('comp1').material('mat4').propertyGroup('KG').func.create('kappa', 'Piecewise');
model.component('comp1').material('mat6').propertyGroup('def').func.create('k_solid_1', 'Piecewise');
model.component('comp1').material('mat6').propertyGroup('def').func.create('alpha_solid_1', 'Piecewise');
model.component('comp1').material('mat6').propertyGroup('def').func.create('C_solid_1', 'Piecewise');
model.component('comp1').material('mat6').propertyGroup('def').func.create('HC_solid_1', 'Piecewise');
model.component('comp1').material('mat6').propertyGroup('def').func.create('VP_solid_1', 'Piecewise');
model.component('comp1').material('mat6').propertyGroup('def').func.create('rho_solid_1', 'Piecewise');
model.component('comp1').material('mat6').propertyGroup('def').func.create('TD_solid_1', 'Piecewise');
model.component('comp1').material('mat6').propertyGroup.create('ThermalExpansion', 'Thermal expansion');
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').func.create('dL_solid_1', 'Piecewise');
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').func.create('CTE_solid_1', 'Piecewise');
model.component('comp1').material('mat6').propertyGroup.create('Enu', 'Young''s modulus and Poisson''s ratio');
model.component('comp1').material('mat6').propertyGroup('Enu').func.create('E_solid_polycrystalline_1', 'Piecewise');
model.component('comp1').material('mat6').propertyGroup('Enu').func.create('nu', 'Piecewise');
model.component('comp1').material('mat6').propertyGroup.create('KG', 'Bulk modulus and shear modulus');
model.component('comp1').material('mat6').propertyGroup('KG').func.create('mu', 'Piecewise');
model.component('comp1').material('mat6').propertyGroup('KG').func.create('kappa', 'Piecewise');

model.component('comp1').common.create('mpf1', 'ParticipationFactors');

model.component('comp1').physics.create('solid', 'SolidMechanics', 'geom1');
model.component('comp1').physics('solid').create('fix1', 'Fixed', 2);
model.component('comp1').physics('solid').feature('fix1').selection.set([3 10]);
model.component('comp1').physics.create('sens', 'Sensitivity', 'geom1');
model.component('comp1').physics('sens').create('gobj1', 'GlobalObjective', -1);
model.component('comp1').physics('sens').create('gcvar1', 'GlobalControlVariables', -1);
model.component('comp1').physics.create('sens2', 'Sensitivity', 'geom1');
model.component('comp1').physics('sens2').create('iobj1', 'IntegralObjective', 3);
model.component('comp1').physics('sens2').feature('iobj1').selection.named('geom1_ext2_dom');
model.component('comp1').physics('sens2').create('gcvar1', 'GlobalControlVariables', -1);

model.component('comp1').mesh('mesh1').create('ftri1', 'FreeTri');
model.component('comp1').mesh('mesh1').create('swe1', 'Sweep');
model.component('comp1').mesh('mesh1').create('swe2', 'Sweep');
model.component('comp1').mesh('mesh1').create('fq1', 'FreeQuad');
model.component('comp1').mesh('mesh1').create('cpf1', 'CopyFace');
model.component('comp1').mesh('mesh1').create('ftet1', 'FreeTet');
model.component('comp1').mesh('mesh1').create('ftet2', 'FreeTet');
model.component('comp1').mesh('mesh1').feature('ftri1').selection.named('geom1_wp1_bnd');
model.component('comp1').mesh('mesh1').feature('swe1').selection.named('geom1_csol1_dom');
model.component('comp1').mesh('mesh1').feature('swe1').create('dis1', 'Distribution');
model.component('comp1').mesh('mesh1').feature('swe2').selection.named('geom1_ext2_dom');
model.component('comp1').mesh('mesh1').feature('swe2').create('dis1', 'Distribution');
model.component('comp1').mesh('mesh1').feature('swe2').feature('dis1').selection.set([]);
model.component('comp1').mesh('mesh1').feature('fq1').selection.remaining;
model.component('comp1').mesh('mesh1').feature('ftet1').selection.named('geom1_ext4_dom');
model.component('comp1').mesh('mesh1').feature('ftet1').create('size1', 'Size');
model.component('comp1').mesh('mesh1').feature('ftet2').selection.named('geom1_ext5_dom');
model.component('comp1').mesh('mesh1').feature('ftet2').create('size1', 'Size');

model.result.table('tbl1').comments('coating SE');
model.result.table('tbl2').comments('clamp SE');
model.result.table('tbl3').comments('Global Evaluation 1');

model.component('comp1').view('view1').hideObjects('hide1').init(2);
model.component('comp1').view('view2').axis.set('xmin', -0.022841926664114);
model.component('comp1').view('view2').axis.set('xmax', 0.03284192830324173);
model.component('comp1').view('view2').axis.set('ymin', -0.00777301425114274);
model.component('comp1').view('view2').axis.set('ymax', 0.03577301651239395);
model.component('comp1').view('view3').axis.set('xmin', -0.0062616500072181225);
model.component('comp1').view('view3').axis.set('xmax', 0.006361649837344885);
model.component('comp1').view('view3').axis.set('ymin', -0.0017875009216368198);
model.component('comp1').view('view3').axis.set('ymax', 0.020787499845027924);
model.component('comp1').view('view4').axis.set('xmin', -0.011287501081824303);
model.component('comp1').view('view4').axis.set('xmax', 0.011287501081824303);
model.component('comp1').view('view4').axis.set('ymin', -0.02153611183166504);
model.component('comp1').view('view4').axis.set('ymax', 0.018836114555597305);
model.component('comp1').view('view5').axis.set('xmin', -0.010921319015324116);
model.component('comp1').view('view5').axis.set('xmax', 0.011021818034350872);
model.component('comp1').view('view5').axis.set('ymin', -9.499993175268173E-4);
model.component('comp1').view('view5').axis.set('ymax', 0.019950000569224358);

model.component('comp1').material('mat1').label('Silicon [solid,<100> axis]');
model.component('comp1').material('mat1').set('family', 'custom');
model.component('comp1').material('mat1').set('customspecular', [0.7843137254901961 1 1]);
model.component('comp1').material('mat1').set('diffuse', 'white');
model.component('comp1').material('mat1').set('fresnel', 0.9);
model.component('comp1').material('mat1').set('roughness', 0.1);
model.component('comp1').material('mat1').set('metallic', 0);
model.component('comp1').material('mat1').set('pearl', 0);
model.component('comp1').material('mat1').set('diffusewrap', 0);
model.component('comp1').material('mat1').set('clearcoat', 0);
model.component('comp1').material('mat1').set('reflectance', 0);
model.component('comp1').material('mat1').propertyGroup('def').func('alpha_solid_1').label('Piecewise 2');
model.component('comp1').material('mat1').propertyGroup('def').func('alpha_solid_1').set('arg', 'T');
model.component('comp1').material('mat1').propertyGroup('def').func('alpha_solid_1').set('pieces', {'0.0' '30.0' '7.35637E-7+2.453566E-9*T^1+1.20482E-11*T^2';  ...
'30.0' '130.0' '7.713685E-7+2.098318E-10*T^1+4.628581E-11*T^2+7.569451E-14*T^3-8.713366E-16*T^4';  ...
'130.0' '293.0' '-3.223163E-7+2.257142E-8*T^1-9.684044E-11*T^2+2.835316E-13*T^3-3.440569E-16*T^4';  ...
'293.0' '1000.0' '6.772622E-7+9.501405E-9*T^1-1.271286E-11*T^2+8.584038E-15*T^3-2.241706E-18*T^4'});
model.component('comp1').material('mat1').propertyGroup('def').func('C_solid_1').label('Piecewise 1');
model.component('comp1').material('mat1').propertyGroup('def').func('C_solid_1').set('arg', 'T');
model.component('comp1').material('mat1').propertyGroup('def').func('C_solid_1').set('pieces', {'1.0' '7.0' '-4.8321811E-5+7.68448084E-5*T^1-3.41813386E-5*T^2+2.80830708E-4*T^3-3.12897302E-7*T^4';  ...
'7.0' '20.0' '0.0525075264-0.0396481488*T^1+0.0100460936*T^2-7.81251542E-4*T^3+3.9615568E-5*T^4';  ...
'20.0' '50.0' '-1.80567549+0.761903471*T^1-0.0865373791*T^2+0.0037353614*T^3-3.33397563E-5*T^4';  ...
'50.0' '293.0' '-82.9482602+2.71223532*T^1+0.0140475122*T^2-7.97769138E-5*T^3+1.07990546E-7*T^4';  ...
'293.0' '900.0' '63.0442191+3.7706731*T^1-0.00694853616*T^2+5.9532044E-6*T^3-1.91438418E-9*T^4';  ...
'900.0' '1685.0' '769.459775+0.187175131*T^1-3.18395957E-5*T^2'});
model.component('comp1').material('mat1').propertyGroup('def').func('HC_solid_1').label('Piecewise 2.1');
model.component('comp1').material('mat1').propertyGroup('def').func('HC_solid_1').set('arg', 'T');
model.component('comp1').material('mat1').propertyGroup('def').func('HC_solid_1').set('pieces', {'1.0' '7.0' '-1.35714274E-6+2.15822519E-6*T^1-9.59999972E-7*T^2+7.88727095E-6*T^3-8.78787695E-9*T^4';  ...
'7.0' '20.0' '0.00147469975-0.00111353813*T^1+2.82149588E-4*T^2-2.19418374E-5*T^3+1.11262309E-6*T^4';  ...
'20.0' '50.0' '-0.0507133017+0.0213984404*T^1-0.00243044543*T^2+1.0490949E-4*T^3-9.36363719E-7*T^4';  ...
'50.0' '293.0' '-2.32964367+0.0761744898*T^1+3.9453141E-4*T^2-2.2405751E-6*T^3+3.03296821E-9*T^4';  ...
'293.0' '900.0' '1.3923419+0.109040605*T^1-2.04395094E-4*T^2+1.78689895E-7*T^3-5.88857415E-11*T^4';  ...
'900.0' '1685.0' '22.0332118+0.0045851661*T^1-6.45226774E-7*T^2'});
model.component('comp1').material('mat1').propertyGroup('def').func('VP_solid_1').label('Piecewise 3');
model.component('comp1').material('mat1').propertyGroup('def').func('VP_solid_1').set('arg', 'T');
model.component('comp1').material('mat1').propertyGroup('def').func('VP_solid_1').set('pieces', {'293.0' '1685.0' '(exp((-2.35500000e+04/T-5.65000000e-01*log10(T)+1.23500000e+01)*log(10.0)))*1.33320000e+02'});
model.component('comp1').material('mat1').propertyGroup('def').func('rho_solid_1').label('Piecewise 4');
model.component('comp1').material('mat1').propertyGroup('def').func('rho_solid_1').set('arg', 'T');
model.component('comp1').material('mat1').propertyGroup('def').func('rho_solid_1').set('pieces', {'0.0' '30.0' '2331.507-7.113612E-5*T^1+3.674386E-6*T^2';  ...
'30.0' '130.0' '2331.592-0.005873649*T^1+1.206114E-4*T^2-5.479876E-7*T^3+1.606517E-10*T^4';  ...
'130.0' '293.0' '2330.436+0.02130626*T^1-9.544145E-5*T^2+4.607415E-8*T^3+4.840886E-11*T^4';  ...
'293.0' '1000.0' '2332.565+0.003839515*T^1-5.433308E-5*T^2+4.287211E-8*T^3-1.366545E-11*T^4'});
model.component('comp1').material('mat1').propertyGroup('def').func('TD').label('Piecewise 5');
model.component('comp1').material('mat1').propertyGroup('def').func('TD').set('arg', 'T');
model.component('comp1').material('mat1').propertyGroup('def').func('TD').set('pieces', {'1.0' '12.0' '12.40366-2.652049*T^1+0.5813105*T^2-0.08018325*T^3+0.005288731*T^4-1.314169E-4*T^5';  ...
'12.0' '36.0' '12.40379-1.240777*T^1+0.03837706*T^2-9.696578E-6*T^3-1.861403E-5*T^4+2.36465E-7*T^5';  ...
'36.0' '70.0' '2.231412-0.1770592*T^1+0.005725565*T^2-9.331438E-5*T^3+7.623105E-7*T^4-2.489437E-9*T^5';  ...
'70.0' '165.0' '0.08907341-0.003083678*T^1+4.419818E-5*T^2-3.22047E-7*T^3+1.182074E-9*T^4-1.739399E-12*T^5';  ...
'165.0' '305.0' '0.01190219-2.287791E-4*T^1+1.824052E-6*T^2-7.358192E-9*T^3+1.487663E-11*T^4-1.200563E-14*T^5';  ...
'305.0' '735.0' '7.622018E-4-5.322058E-6*T^1+1.632442E-8*T^2-2.584688E-11*T^3+2.077321E-14*T^4-6.740584E-18*T^5';  ...
'735.0' '1000.0' '-2.043535E-4+1.357482E-6*T^1-2.753022E-9*T^2+2.336409E-12*T^3-7.219021E-16*T^4'});
model.component('comp1').material('mat1').propertyGroup('def').set('thermalexpansioncoefficient', {'(alpha_solid_1(T[1/K])[1/K]+(Tempref-293[K])*if(abs(T-Tempref)>1e-3,(alpha_solid_1(T[1/K])[1/K]-alpha_solid_1(Tempref[1/K])[1/K])/(T-Tempref),d(alpha_solid_1(T[1/K])[1/K],T)))/(1+alpha_solid_1(Tempref[1/K])[1/K]*(Tempref-293[K]))' '0' '0' '0' '(alpha_solid_1(T[1/K])[1/K]+(Tempref-293[K])*if(abs(T-Tempref)>1e-3,(alpha_solid_1(T[1/K])[1/K]-alpha_solid_1(Tempref[1/K])[1/K])/(T-Tempref),d(alpha_solid_1(T[1/K])[1/K],T)))/(1+alpha_solid_1(Tempref[1/K])[1/K]*(Tempref-293[K]))' '0' '0' '0' '(alpha_solid_1(T[1/K])[1/K]+(Tempref-293[K])*if(abs(T-Tempref)>1e-3,(alpha_solid_1(T[1/K])[1/K]-alpha_solid_1(Tempref[1/K])[1/K])/(T-Tempref),d(alpha_solid_1(T[1/K])[1/K],T)))/(1+alpha_solid_1(Tempref[1/K])[1/K]*(Tempref-293[K]))'});
model.component('comp1').material('mat1').propertyGroup('def').set('INFO_PREFIX:thermalexpansioncoefficient', ['Reference: C.A. Swenson, Journal of Physical and Chemical Reference Data, v12, No. 2, p179 (1983) available online at https://srd.nist.gov/JPCRD/jpcrd220.pdf\nNote: the reference temperature is 20C (293K), less than 1% error\nReference temperature: 293.00[K]']);
model.component('comp1').material('mat1').propertyGroup('def').set('heatcapacity', 'C_solid_1(T[1/K])[J/(kg*K)]');
model.component('comp1').material('mat1').propertyGroup('def').set('INFO_PREFIX:heatcapacity', ['Reference: P.D. Desai, Journal of Physical and Chemical Reference Data, v15, No. 3, p967 (1986) available online at https://srd.nist.gov/JPCRD/jpcrd298.pdf and K.K. Kelley, US Bureau of Mines, Bulletin No. 476 (1949) available online at http://pbadupws.nrc.gov/docs/ML1212/ML12124A257.pdf\nNote: 1.5 to 5% error']);
model.component('comp1').material('mat1').propertyGroup('def').set('HC', 'HC_solid_1(T[1/K])[J/(mol*K)]');
model.component('comp1').material('mat1').propertyGroup('def').set('INFO_PREFIX:HC', ['Reference: P.D. Desai, Journal of Physical and Chemical Reference Data, v15, No. 3, p967 (1986) available online at https://srd.nist.gov/JPCRD/jpcrd298.pdf and K.K. Kelley, US Bureau of Mines, Bulletin No. 476 (1949) available online at http://pbadupws.nrc.gov/docs/ML1212/ML12124A257.pdf\nNote: 1.5 to 5% error']);
model.component('comp1').material('mat1').propertyGroup('def').set('VP', 'VP_solid_1(T[1/K])[Pa]');
model.component('comp1').material('mat1').propertyGroup('def').set('INFO_PREFIX:VP', 'Reference: Metallurgical Thermochemistry, 5th Edition, O. Kubasschewski and C.B. Alcock, Pergamon Press (1979)');
model.component('comp1').material('mat1').propertyGroup('def').set('density', 'rho_solid_1(T[1/K])[kg/m^3]');
model.component('comp1').material('mat1').propertyGroup('def').set('INFO_PREFIX:density', ['Reference: C.A. Swenson, Journal of Physical and Chemical Reference Data, v12, No. 2, p179 (1983) available online at https://srd.nist.gov/JPCRD/jpcrd220.pdf\nNote: less than 1% error']);
model.component('comp1').material('mat1').propertyGroup('def').set('TD', 'TD(T[1/K])[m^2/s]');
model.component('comp1').material('mat1').propertyGroup('def').set('INFO_PREFIX:TD', 'Reference: calculated from the thermal conductivity, density and specific heat');
model.component('comp1').material('mat1').propertyGroup('def').set('lossfactor', '1e-8');
model.component('comp1').material('mat1').propertyGroup('def').addInput('temperature');
model.component('comp1').material('mat1').propertyGroup('def').addInput('strainreferencetemperature');
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').info('category').body('Solid_mechanics/');
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').func('dL_solid_1').label('Piecewise 2');
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').func('dL_solid_1').set('arg', 'T');
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').func('dL_solid_1').set('pieces', {'0.0' '30.0' '-2.154537E-4';  ...
'30.0' '130.0' '-2.276144E-4+8.396104E-7*T^1-1.724143E-8*T^2+7.834799E-11*T^3-2.303956E-14*T^4';  ...
'130.0' '293.0' '-5.217231E-5-3.263667E-6*T^1+1.532991E-8*T^2-1.223001E-11*T^3';  ...
'293.0' '1000.0' '-5.844527E-4+1.124129E-6*T^1+3.311476E-9*T^2-1.161022E-12*T^3'});
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').func('CTE').label('Piecewise 1');
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').func('CTE').set('arg', 'T');
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').func('CTE').set('pieces', {'0.0' '35.0' '-1.128708E-10+4.013852E-10*T^1-1.52474E-10*T^2+1.935922E-11*T^3-8.512984E-13*T^4+9.848876E-15*T^5';  ...
'35.0' '121.0' '-4.536302E-7+5.088778E-8*T^1-1.847617E-9*T^2+2.348401E-11*T^3-1.242463E-13*T^4+2.442997E-16*T^5';  ...
'121.0' '293.0' '8.574679E-8-3.514147E-8*T^1+4.332654E-10*T^2-1.449023E-12*T^3+1.630929E-15*T^4';  ...
'293.0' '1000.0' '-2.621356E-6+3.020773E-8*T^1-5.538848E-11*T^2+4.755059E-14*T^3-1.534596E-17*T^4'});
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').set('alphatan', '');
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').set('dL', '');
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').set('alphatan', {'CTE(T[1/K])[1/K]' '0' '0' '0' 'CTE(T[1/K])[1/K]' '0' '0' '0' 'CTE(T[1/K])[1/K]'});
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').set('INFO_PREFIX:alphatan', ['Reference: C.A. Swenson, Journal of Physical and Chemical Reference Data, v12, No. 2, p179 (1983) available online at https://srd.nist.gov/JPCRD/jpcrd220.pdf\nNote: less than 1% error']);
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').set('dL', {'(dL_solid_1(T[1/K])-dL_solid_1(Tempref[1/K]))/(1+dL_solid_1(Tempref[1/K]))' '0' '0' '0' '(dL_solid_1(T[1/K])-dL_solid_1(Tempref[1/K]))/(1+dL_solid_1(Tempref[1/K]))' '0' '0' '0' '(dL_solid_1(T[1/K])-dL_solid_1(Tempref[1/K]))/(1+dL_solid_1(Tempref[1/K]))'});
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').set('INFO_PREFIX:dL', ['Reference: C.A. Swenson, Journal of Physical and Chemical Reference Data, v12, No. 2, p179 (1983) available online at https://srd.nist.gov/JPCRD/jpcrd220.pdf\nNote: the reference temperature is 20C (293K), less than 1% error\nReference temperature: 293.00[K]']);
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').addInput('temperature');
model.component('comp1').material('mat1').propertyGroup('ThermalExpansion').addInput('strainreferencetemperature');
model.component('comp1').material('mat1').propertyGroup('Enu').func('E_solid_100_axis_2').label('Piecewise');
model.component('comp1').material('mat1').propertyGroup('Enu').func('E_solid_100_axis_2').set('arg', 'T');
model.component('comp1').material('mat1').propertyGroup('Enu').func('E_solid_100_axis_2').set('pieces', {'73.0' '923.0' '1.310854E11+2223259.0*T^1-21955.19*T^2+16.60284*T^3-0.005400611*T^4'});
model.component('comp1').material('mat1').propertyGroup('Enu').func('nu_solid_100_axis_2').label('Piecewise 1');
model.component('comp1').material('mat1').propertyGroup('Enu').func('nu_solid_100_axis_2').set('arg', 'T');
model.component('comp1').material('mat1').propertyGroup('Enu').func('nu_solid_100_axis_2').set('pieces', {'73.0' '923.0' '0.2794053-4.6827E-6*T^1'});
model.component('comp1').material('mat1').propertyGroup('Enu').set('E', '');
model.component('comp1').material('mat1').propertyGroup('Enu').set('nu', '');
model.component('comp1').material('mat1').propertyGroup('Enu').set('E', 'E_solid_100_axis_2(T[1/K])[Pa]');
model.component('comp1').material('mat1').propertyGroup('Enu').set('nu', 'nu_solid_100_axis_2(T[1/K])');
model.component('comp1').material('mat1').propertyGroup('Enu').addInput('temperature');
model.component('comp1').material('mat1').propertyGroup('KG').func('mu_solid_100_axis_2').label('Piecewise');
model.component('comp1').material('mat1').propertyGroup('KG').func('mu_solid_100_axis_2').set('arg', 'T');
model.component('comp1').material('mat1').propertyGroup('KG').func('mu_solid_100_axis_2').set('pieces', {'73.0' '923.0' '8.011001E10-93254.81*T^1-4706.652*T^2'});
model.component('comp1').material('mat1').propertyGroup('KG').set('K', '');
model.component('comp1').material('mat1').propertyGroup('KG').set('G', 'mu_solid_100_axis_2(T[1/K])[Pa]');
model.component('comp1').material('mat1').propertyGroup('KG').set('INFO_PREFIX:G', ['Reference: H. Over, O. Knotek and E. Lugscheider, Zeitschrift fuer Metallkunde, v73, No. 9, p552 (1982) and H.J. McSkimin, Journal of Applied Physics, v24, No. 8, p988 (1953)\nNote: calculated from elastic constants']);
model.component('comp1').material('mat1').propertyGroup('KG').addInput('temperature');
model.material('mat2').label('Silicon [solid,<100> axis] 1');
model.material('mat2').set('family', 'custom');
model.material('mat2').set('customspecular', [0.7843137254901961 1 1]);
model.material('mat2').set('diffuse', 'white');
model.material('mat2').set('fresnel', 0.9);
model.material('mat2').set('roughness', 0.1);
model.material('mat2').set('metallic', 0);
model.material('mat2').set('pearl', 0);
model.material('mat2').set('diffusewrap', 0);
model.material('mat2').set('clearcoat', 0);
model.material('mat2').set('reflectance', 0);
model.material('mat2').propertyGroup('def').func('alpha_solid_1').label('Piecewise 2');
model.material('mat2').propertyGroup('def').func('alpha_solid_1').set('arg', 'T');
model.material('mat2').propertyGroup('def').func('alpha_solid_1').set('pieces', {'0.0' '30.0' '7.35637E-7+2.453566E-9*T^1+1.20482E-11*T^2';  ...
'30.0' '130.0' '7.713685E-7+2.098318E-10*T^1+4.628581E-11*T^2+7.569451E-14*T^3-8.713366E-16*T^4';  ...
'130.0' '293.0' '-3.223163E-7+2.257142E-8*T^1-9.684044E-11*T^2+2.835316E-13*T^3-3.440569E-16*T^4';  ...
'293.0' '1000.0' '6.772622E-7+9.501405E-9*T^1-1.271286E-11*T^2+8.584038E-15*T^3-2.241706E-18*T^4'});
model.material('mat2').propertyGroup('def').func('C_solid_1').label('Piecewise 1');
model.material('mat2').propertyGroup('def').func('C_solid_1').set('arg', 'T');
model.material('mat2').propertyGroup('def').func('C_solid_1').set('pieces', {'1.0' '7.0' '-4.8321811E-5+7.68448084E-5*T^1-3.41813386E-5*T^2+2.80830708E-4*T^3-3.12897302E-7*T^4';  ...
'7.0' '20.0' '0.0525075264-0.0396481488*T^1+0.0100460936*T^2-7.81251542E-4*T^3+3.9615568E-5*T^4';  ...
'20.0' '50.0' '-1.80567549+0.761903471*T^1-0.0865373791*T^2+0.0037353614*T^3-3.33397563E-5*T^4';  ...
'50.0' '293.0' '-82.9482602+2.71223532*T^1+0.0140475122*T^2-7.97769138E-5*T^3+1.07990546E-7*T^4';  ...
'293.0' '900.0' '63.0442191+3.7706731*T^1-0.00694853616*T^2+5.9532044E-6*T^3-1.91438418E-9*T^4';  ...
'900.0' '1685.0' '769.459775+0.187175131*T^1-3.18395957E-5*T^2'});
model.material('mat2').propertyGroup('def').func('HC_solid_1').label('Piecewise 2.1');
model.material('mat2').propertyGroup('def').func('HC_solid_1').set('arg', 'T');
model.material('mat2').propertyGroup('def').func('HC_solid_1').set('pieces', {'1.0' '7.0' '-1.35714274E-6+2.15822519E-6*T^1-9.59999972E-7*T^2+7.88727095E-6*T^3-8.78787695E-9*T^4';  ...
'7.0' '20.0' '0.00147469975-0.00111353813*T^1+2.82149588E-4*T^2-2.19418374E-5*T^3+1.11262309E-6*T^4';  ...
'20.0' '50.0' '-0.0507133017+0.0213984404*T^1-0.00243044543*T^2+1.0490949E-4*T^3-9.36363719E-7*T^4';  ...
'50.0' '293.0' '-2.32964367+0.0761744898*T^1+3.9453141E-4*T^2-2.2405751E-6*T^3+3.03296821E-9*T^4';  ...
'293.0' '900.0' '1.3923419+0.109040605*T^1-2.04395094E-4*T^2+1.78689895E-7*T^3-5.88857415E-11*T^4';  ...
'900.0' '1685.0' '22.0332118+0.0045851661*T^1-6.45226774E-7*T^2'});
model.material('mat2').propertyGroup('def').func('VP_solid_1').label('Piecewise 3');
model.material('mat2').propertyGroup('def').func('VP_solid_1').set('arg', 'T');
model.material('mat2').propertyGroup('def').func('VP_solid_1').set('pieces', {'293.0' '1685.0' '(exp((-2.35500000e+04/T-5.65000000e-01*log10(T)+1.23500000e+01)*log(10.0)))*1.33320000e+02'});
model.material('mat2').propertyGroup('def').func('rho_solid_1').label('Piecewise 4');
model.material('mat2').propertyGroup('def').func('rho_solid_1').set('arg', 'T');
model.material('mat2').propertyGroup('def').func('rho_solid_1').set('pieces', {'0.0' '30.0' '2331.507-7.113612E-5*T^1+3.674386E-6*T^2';  ...
'30.0' '130.0' '2331.592-0.005873649*T^1+1.206114E-4*T^2-5.479876E-7*T^3+1.606517E-10*T^4';  ...
'130.0' '293.0' '2330.436+0.02130626*T^1-9.544145E-5*T^2+4.607415E-8*T^3+4.840886E-11*T^4';  ...
'293.0' '1000.0' '2332.565+0.003839515*T^1-5.433308E-5*T^2+4.287211E-8*T^3-1.366545E-11*T^4'});
model.material('mat2').propertyGroup('def').func('TD').label('Piecewise 5');
model.material('mat2').propertyGroup('def').func('TD').set('arg', 'T');
model.material('mat2').propertyGroup('def').func('TD').set('pieces', {'1.0' '12.0' '12.40366-2.652049*T^1+0.5813105*T^2-0.08018325*T^3+0.005288731*T^4-1.314169E-4*T^5';  ...
'12.0' '36.0' '12.40379-1.240777*T^1+0.03837706*T^2-9.696578E-6*T^3-1.861403E-5*T^4+2.36465E-7*T^5';  ...
'36.0' '70.0' '2.231412-0.1770592*T^1+0.005725565*T^2-9.331438E-5*T^3+7.623105E-7*T^4-2.489437E-9*T^5';  ...
'70.0' '165.0' '0.08907341-0.003083678*T^1+4.419818E-5*T^2-3.22047E-7*T^3+1.182074E-9*T^4-1.739399E-12*T^5';  ...
'165.0' '305.0' '0.01190219-2.287791E-4*T^1+1.824052E-6*T^2-7.358192E-9*T^3+1.487663E-11*T^4-1.200563E-14*T^5';  ...
'305.0' '735.0' '7.622018E-4-5.322058E-6*T^1+1.632442E-8*T^2-2.584688E-11*T^3+2.077321E-14*T^4-6.740584E-18*T^5';  ...
'735.0' '1000.0' '-2.043535E-4+1.357482E-6*T^1-2.753022E-9*T^2+2.336409E-12*T^3-7.219021E-16*T^4'});
model.material('mat2').propertyGroup('def').set('thermalexpansioncoefficient', {'(alpha_solid_1(T[1/K])[1/K]+(Tempref-293[K])*if(abs(T-Tempref)>1e-3,(alpha_solid_1(T[1/K])[1/K]-alpha_solid_1(Tempref[1/K])[1/K])/(T-Tempref),d(alpha_solid_1(T[1/K])[1/K],T)))/(1+alpha_solid_1(Tempref[1/K])[1/K]*(Tempref-293[K]))' '0' '0' '0' '(alpha_solid_1(T[1/K])[1/K]+(Tempref-293[K])*if(abs(T-Tempref)>1e-3,(alpha_solid_1(T[1/K])[1/K]-alpha_solid_1(Tempref[1/K])[1/K])/(T-Tempref),d(alpha_solid_1(T[1/K])[1/K],T)))/(1+alpha_solid_1(Tempref[1/K])[1/K]*(Tempref-293[K]))' '0' '0' '0' '(alpha_solid_1(T[1/K])[1/K]+(Tempref-293[K])*if(abs(T-Tempref)>1e-3,(alpha_solid_1(T[1/K])[1/K]-alpha_solid_1(Tempref[1/K])[1/K])/(T-Tempref),d(alpha_solid_1(T[1/K])[1/K],T)))/(1+alpha_solid_1(Tempref[1/K])[1/K]*(Tempref-293[K]))'});
model.material('mat2').propertyGroup('def').set('INFO_PREFIX:thermalexpansioncoefficient', ['Reference: C.A. Swenson, Journal of Physical and Chemical Reference Data, v12, No. 2, p179 (1983) available online at https://srd.nist.gov/JPCRD/jpcrd220.pdf\nNote: the reference temperature is 20C (293K), less than 1% error\nReference temperature: 293.00[K]']);
model.material('mat2').propertyGroup('def').set('heatcapacity', 'C_solid_1(T[1/K])[J/(kg*K)]');
model.material('mat2').propertyGroup('def').set('INFO_PREFIX:heatcapacity', ['Reference: P.D. Desai, Journal of Physical and Chemical Reference Data, v15, No. 3, p967 (1986) available online at https://srd.nist.gov/JPCRD/jpcrd298.pdf and K.K. Kelley, US Bureau of Mines, Bulletin No. 476 (1949) available online at http://pbadupws.nrc.gov/docs/ML1212/ML12124A257.pdf\nNote: 1.5 to 5% error']);
model.material('mat2').propertyGroup('def').set('HC', 'HC_solid_1(T[1/K])[J/(mol*K)]');
model.material('mat2').propertyGroup('def').set('INFO_PREFIX:HC', ['Reference: P.D. Desai, Journal of Physical and Chemical Reference Data, v15, No. 3, p967 (1986) available online at https://srd.nist.gov/JPCRD/jpcrd298.pdf and K.K. Kelley, US Bureau of Mines, Bulletin No. 476 (1949) available online at http://pbadupws.nrc.gov/docs/ML1212/ML12124A257.pdf\nNote: 1.5 to 5% error']);
model.material('mat2').propertyGroup('def').set('VP', 'VP_solid_1(T[1/K])[Pa]');
model.material('mat2').propertyGroup('def').set('INFO_PREFIX:VP', 'Reference: Metallurgical Thermochemistry, 5th Edition, O. Kubasschewski and C.B. Alcock, Pergamon Press (1979)');
model.material('mat2').propertyGroup('def').set('density', 'rho_solid_1(T[1/K])[kg/m^3]');
model.material('mat2').propertyGroup('def').set('INFO_PREFIX:density', ['Reference: C.A. Swenson, Journal of Physical and Chemical Reference Data, v12, No. 2, p179 (1983) available online at https://srd.nist.gov/JPCRD/jpcrd220.pdf\nNote: less than 1% error']);
model.material('mat2').propertyGroup('def').set('TD', 'TD(T[1/K])[m^2/s]');
model.material('mat2').propertyGroup('def').set('INFO_PREFIX:TD', 'Reference: calculated from the thermal conductivity, density and specific heat');
model.material('mat2').propertyGroup('def').addInput('temperature');
model.material('mat2').propertyGroup('def').addInput('strainreferencetemperature');
model.material('mat2').propertyGroup('ThermalExpansion').info('category').body('Solid_mechanics/');
model.material('mat2').propertyGroup('ThermalExpansion').func('dL_solid_1').label('Piecewise 2');
model.material('mat2').propertyGroup('ThermalExpansion').func('dL_solid_1').set('arg', 'T');
model.material('mat2').propertyGroup('ThermalExpansion').func('dL_solid_1').set('pieces', {'0.0' '30.0' '-2.154537E-4';  ...
'30.0' '130.0' '-2.276144E-4+8.396104E-7*T^1-1.724143E-8*T^2+7.834799E-11*T^3-2.303956E-14*T^4';  ...
'130.0' '293.0' '-5.217231E-5-3.263667E-6*T^1+1.532991E-8*T^2-1.223001E-11*T^3';  ...
'293.0' '1000.0' '-5.844527E-4+1.124129E-6*T^1+3.311476E-9*T^2-1.161022E-12*T^3'});
model.material('mat2').propertyGroup('ThermalExpansion').func('CTE').label('Piecewise 1');
model.material('mat2').propertyGroup('ThermalExpansion').func('CTE').set('arg', 'T');
model.material('mat2').propertyGroup('ThermalExpansion').func('CTE').set('pieces', {'0.0' '35.0' '-1.128708E-10+4.013852E-10*T^1-1.52474E-10*T^2+1.935922E-11*T^3-8.512984E-13*T^4+9.848876E-15*T^5';  ...
'35.0' '121.0' '-4.536302E-7+5.088778E-8*T^1-1.847617E-9*T^2+2.348401E-11*T^3-1.242463E-13*T^4+2.442997E-16*T^5';  ...
'121.0' '293.0' '8.574679E-8-3.514147E-8*T^1+4.332654E-10*T^2-1.449023E-12*T^3+1.630929E-15*T^4';  ...
'293.0' '1000.0' '-2.621356E-6+3.020773E-8*T^1-5.538848E-11*T^2+4.755059E-14*T^3-1.534596E-17*T^4'});
model.material('mat2').propertyGroup('ThermalExpansion').set('alphatan', '');
model.material('mat2').propertyGroup('ThermalExpansion').set('dL', '');
model.material('mat2').propertyGroup('ThermalExpansion').set('alphatan', {'CTE(T[1/K])[1/K]' '0' '0' '0' 'CTE(T[1/K])[1/K]' '0' '0' '0' 'CTE(T[1/K])[1/K]'});
model.material('mat2').propertyGroup('ThermalExpansion').set('INFO_PREFIX:alphatan', ['Reference: C.A. Swenson, Journal of Physical and Chemical Reference Data, v12, No. 2, p179 (1983) available online at https://srd.nist.gov/JPCRD/jpcrd220.pdf\nNote: less than 1% error']);
model.material('mat2').propertyGroup('ThermalExpansion').set('dL', {'(dL_solid_1(T[1/K])-dL_solid_1(Tempref[1/K]))/(1+dL_solid_1(Tempref[1/K]))' '0' '0' '0' '(dL_solid_1(T[1/K])-dL_solid_1(Tempref[1/K]))/(1+dL_solid_1(Tempref[1/K]))' '0' '0' '0' '(dL_solid_1(T[1/K])-dL_solid_1(Tempref[1/K]))/(1+dL_solid_1(Tempref[1/K]))'});
model.material('mat2').propertyGroup('ThermalExpansion').set('INFO_PREFIX:dL', ['Reference: C.A. Swenson, Journal of Physical and Chemical Reference Data, v12, No. 2, p179 (1983) available online at https://srd.nist.gov/JPCRD/jpcrd220.pdf\nNote: the reference temperature is 20C (293K), less than 1% error\nReference temperature: 293.00[K]']);
model.material('mat2').propertyGroup('ThermalExpansion').addInput('temperature');
model.material('mat2').propertyGroup('ThermalExpansion').addInput('strainreferencetemperature');
model.material('mat2').propertyGroup('Enu').func('E_solid_100_axis_2').label('Piecewise');
model.material('mat2').propertyGroup('Enu').func('E_solid_100_axis_2').set('arg', 'T');
model.material('mat2').propertyGroup('Enu').func('E_solid_100_axis_2').set('pieces', {'73.0' '923.0' '1.310854E11+2223259.0*T^1-21955.19*T^2+16.60284*T^3-0.005400611*T^4'});
model.material('mat2').propertyGroup('Enu').func('nu_solid_100_axis_2').label('Piecewise 1');
model.material('mat2').propertyGroup('Enu').func('nu_solid_100_axis_2').set('arg', 'T');
model.material('mat2').propertyGroup('Enu').func('nu_solid_100_axis_2').set('pieces', {'73.0' '923.0' '0.2794053-4.6827E-6*T^1'});
model.material('mat2').propertyGroup('Enu').set('E', '');
model.material('mat2').propertyGroup('Enu').set('nu', '');
model.material('mat2').propertyGroup('Enu').set('E', 'E_solid_100_axis_2(T[1/K])[Pa]');
model.material('mat2').propertyGroup('Enu').set('nu', 'nu_solid_100_axis_2(T[1/K])');
model.material('mat2').propertyGroup('Enu').addInput('temperature');
model.material('mat2').propertyGroup('KG').func('mu_solid_100_axis_2').label('Piecewise');
model.material('mat2').propertyGroup('KG').func('mu_solid_100_axis_2').set('arg', 'T');
model.material('mat2').propertyGroup('KG').func('mu_solid_100_axis_2').set('pieces', {'73.0' '923.0' '8.011001E10-93254.81*T^1-4706.652*T^2'});
model.material('mat2').propertyGroup('KG').set('K', '');
model.material('mat2').propertyGroup('KG').set('G', 'mu_solid_100_axis_2(T[1/K])[Pa]');
model.material('mat2').propertyGroup('KG').set('INFO_PREFIX:G', ['Reference: H. Over, O. Knotek and E. Lugscheider, Zeitschrift fuer Metallkunde, v73, No. 9, p552 (1982) and H.J. McSkimin, Journal of Applied Physics, v24, No. 8, p988 (1953)\nNote: calculated from elastic constants']);
model.material('mat2').propertyGroup('KG').addInput('temperature');
model.component('comp1').material('mat4').label('BS B500B (steel reinforcing bar) [solid]');
model.component('comp1').material('mat4').set('family', 'iron');
model.component('comp1').material('mat4').set('info', {'Composition' '' 'bal. Fe, 0.25 C max, 0.05 P max, 0.05 S max, 0.012 N max (wt%)'});
model.component('comp1').material('mat4').propertyGroup('def').func('alpha').set('arg', 'T');
model.component('comp1').material('mat4').propertyGroup('def').func('alpha').set('pieces', {'0.0' '680.0' '6.731035E-6+3.918624E-8*T^1-1.534406E-10*T^2+3.517965E-13*T^3-3.827522E-16*T^4+1.565546E-19*T^5';  ...
'680.0' '977.0' '8.614185E-6+1.013631E-8*T^1-2.26181E-12*T^2-1.569696E-15*T^3';  ...
'977.0' '1061.0' '6.292031E-5-4.915637E-8*T^1';  ...
'1061.0' '1144.0' '-1.563678E-6+1.162025E-8*T^1'});
model.component('comp1').material('mat4').propertyGroup('def').func('C').label('Piecewise 1');
model.component('comp1').material('mat4').propertyGroup('def').func('C').set('arg', 'T');
model.component('comp1').material('mat4').propertyGroup('def').func('C').set('pieces', {'293.0' '948.0' '390.15549+0.360644608*T^1-4.47692602E-4*T^2+6.0790633E-7*T^3'});
model.component('comp1').material('mat4').propertyGroup('def').func('rho').label('Piecewise 2');
model.component('comp1').material('mat4').propertyGroup('def').func('rho').set('arg', 'T');
model.component('comp1').material('mat4').propertyGroup('def').func('rho').set('pieces', {'0.0' '60.0' '7907.978-0.01549395*T^1';  ...
'60.0' '977.0' '7911.3-0.01678428*T^1-8.018711E-4*T^2+1.172796E-6*T^3-1.015971E-9*T^4+3.677737E-13*T^5';  ...
'977.0' '1061.0' '7116.994+0.5195388*T^1';  ...
'1061.0' '1144.0' '8166.523-0.4696497*T^1'});
model.component('comp1').material('mat4').propertyGroup('def').set('thermalexpansioncoefficient', {'(alpha(T[1/K])[1/K]+(Tempref-293[K])*if(abs(T-Tempref)>1e-3,(alpha(T[1/K])[1/K]-alpha(Tempref[1/K])[1/K])/(T-Tempref),d(alpha(T[1/K])[1/K],T)))/(1+alpha(Tempref[1/K])[1/K]*(Tempref-293[K]))' '0' '0' '0' '(alpha(T[1/K])[1/K]+(Tempref-293[K])*if(abs(T-Tempref)>1e-3,(alpha(T[1/K])[1/K]-alpha(Tempref[1/K])[1/K])/(T-Tempref),d(alpha(T[1/K])[1/K],T)))/(1+alpha(Tempref[1/K])[1/K]*(Tempref-293[K]))' '0' '0' '0' '(alpha(T[1/K])[1/K]+(Tempref-293[K])*if(abs(T-Tempref)>1e-3,(alpha(T[1/K])[1/K]-alpha(Tempref[1/K])[1/K])/(T-Tempref),d(alpha(T[1/K])[1/K],T)))/(1+alpha(Tempref[1/K])[1/K]*(Tempref-293[K]))'});
model.component('comp1').material('mat4').propertyGroup('def').set('INFO_PREFIX:thermalexpansioncoefficient', ['Reference: MIL-HDBK-5H, 1 Dec 1998 p2-9 and R.J. Corruccini, J.J. Gniewek, NBS Monograph 29 (1961) available online at http://digicoll.manoa.hawaii.edu/techreports/PDF/NBS29.pdf\nNote: the reference temperature is 20C (293K), same data as 1025 steel\nReference temperature: 293.00[K]']);
model.component('comp1').material('mat4').propertyGroup('def').set('heatcapacity', 'C(T[1/K])[J/(kg*K)]');
model.component('comp1').material('mat4').propertyGroup('def').set('INFO_PREFIX:heatcapacity', ['Reference: High-Temperature Property Data: Ferrous Alloys, Editor M.F. Rothman, ASM International (1988) and Engineering Properties of Steel, P.D. Harvey, ASM (1982)\nNote: mean apparent value']);
model.component('comp1').material('mat4').propertyGroup('def').set('density', 'rho(T[1/K])[kg/m^3]');
model.component('comp1').material('mat4').propertyGroup('def').set('INFO_PREFIX:density', ['Reference: MIL-HDBK-5H, 1 Dec 1998 p2-9 and R.J. Corruccini, J.J. Gniewek, NBS Monograph 29 (1961) available online at http://digicoll.manoa.hawaii.edu/techreports/PDF/NBS29.pdf\nNote: same data as 1025 steel, calculated from the mean coefficient of thermal expansion']);
model.component('comp1').material('mat4').propertyGroup('def').set('lossfactor', '.5');
model.component('comp1').material('mat4').propertyGroup('def').addInput('temperature');
model.component('comp1').material('mat4').propertyGroup('def').addInput('strainreferencetemperature');
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').info('category').body('Solid_mechanics/');
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').func('dL').set('arg', 'T');
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').func('dL').set('pieces', {'0.0' '50.0' '-0.002022973+3.918919E-7*T^1';  ...
'50.0' '212.0' '-0.002063267-3.508062E-7*T^1+2.838477E-8*T^2';  ...
'212.0' '977.0' '-0.00252395773+5.644256E-6*T^1+1.0799E-8*T^2-1.801872E-12*T^3-1.569701E-15*T^4';  ...
'977.0' '1061.0' '0.03264232-2.297313E-5*T^1';  ...
'1061.0' '1144.0' '-0.01351301+2.053771E-5*T^1'});
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').func('CTE').label('Piecewise 1');
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').func('CTE').set('arg', 'T');
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').func('CTE').set('pieces', {'293.0' '1065.0' '5.044861E-6+2.5188E-8*T^1-1.418195E-11*T^2'});
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').set('alphatan', '');
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').set('dL', '');
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').set('alphatan', {'CTE(T[1/K])[1/K]' '0' '0' '0' 'CTE(T[1/K])[1/K]' '0' '0' '0' 'CTE(T[1/K])[1/K]'});
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').set('INFO_PREFIX:alphatan', ['Reference: MIL-HDBK-5H, 1 Dec 1998 p2-9 and R.J. Corruccini, J.J. Gniewek, NBS Monograph 29 (1961) available online at http://digicoll.manoa.hawaii.edu/techreports/PDF/NBS29.pdf\nNote: same data as 1025 steel, calculated from the mean coefficient of thermal expansion']);
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').set('dL', {'(dL(T[1/K])-dL(Tempref[1/K]))/(1+dL(Tempref[1/K]))' '0' '0' '0' '(dL(T[1/K])-dL(Tempref[1/K]))/(1+dL(Tempref[1/K]))' '0' '0' '0' '(dL(T[1/K])-dL(Tempref[1/K]))/(1+dL(Tempref[1/K]))'});
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').set('INFO_PREFIX:dL', ['Reference: MIL-HDBK-5H, 1 Dec 1998 p2-9 and R.J. Corruccini, J.J. Gniewek, NBS Monograph 29 (1961) available online at http://digicoll.manoa.hawaii.edu/techreports/PDF/NBS29.pdf\nNote: the reference temperature is 20C (293K), same data as 1025 steel, calculated from the mean coefficient of thermal expansion\nReference temperature: 293.00[K]']);
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').addInput('temperature');
model.component('comp1').material('mat4').propertyGroup('ThermalExpansion').addInput('strainreferencetemperature');
model.component('comp1').material('mat4').propertyGroup('Enu').func('E').set('arg', 'T');
model.component('comp1').material('mat4').propertyGroup('Enu').func('E').set('pieces', {'4.0' '273.0' '2.217366E11+5020008.0*T^1-305140.4*T^2+926.6601*T^3-1.145454*T^4'; '273.0' '1050.0' '2.109875E11+3.572844E7*T^1-106319.6*T^2'; '1050.0' '1500.0' '2.024261E11-6.77381E7*T^1'});
model.component('comp1').material('mat4').propertyGroup('Enu').func('nu').label('Piecewise 1');
model.component('comp1').material('mat4').propertyGroup('Enu').func('nu').set('arg', 'T');
model.component('comp1').material('mat4').propertyGroup('Enu').func('nu').set('pieces', {'4.0' '120.0' '0.2850355-1.662951E-6*T^1';  ...
'120.0' '273.0' '0.2848011-7.147353E-6*T^1+6.558945E-8*T^2';  ...
'273.0' '1053.0' '0.2712267+7.030261E-5*T^1-3.856929E-8*T^2+1.246582E-11*T^3';  ...
'1053.0' '1500.0' '0.3165268-1.242823E-6*T^1+1.661461E-9*T^2'});
model.component('comp1').material('mat4').propertyGroup('Enu').set('E', '');
model.component('comp1').material('mat4').propertyGroup('Enu').set('nu', '');
model.component('comp1').material('mat4').propertyGroup('Enu').set('E', 'E(T[1/K])[Pa]');
model.component('comp1').material('mat4').propertyGroup('Enu').set('nu', 'nu(T[1/K])');
model.component('comp1').material('mat4').propertyGroup('Enu').addInput('temperature');
model.component('comp1').material('mat4').propertyGroup('KG').func('mu').set('arg', 'T');
model.component('comp1').material('mat4').propertyGroup('KG').func('mu').set('pieces', {'4.0' '273.0' '8.626526E10+1636497.0*T^1-108981.6*T^2+291.1261*T^3-0.3377859*T^4'; '273.0' '1050.0' '8.301552E10+9184755.0*T^1-38834.5*T^2'; '1050.0' '1500.0' '7.694034E10-2.580357E7*T^1'});
model.component('comp1').material('mat4').propertyGroup('KG').func('kappa').label('Piecewise 1');
model.component('comp1').material('mat4').propertyGroup('KG').func('kappa').set('arg', 'T');
model.component('comp1').material('mat4').propertyGroup('KG').func('kappa').set('pieces', {'4.0' '100.0' '1.800573E11-3478717.0*T^1+151512.1*T^2-5485.327*T^3+28.28329*T^4'; '100.0' '273.0' '1.818111E11-4.023792E7*T^1+84204.68*T^2-93.09453*T^3'; '273.0' '1500.0' '1.842649E11-2.509462E7*T^1-28588.37*T^2'});
model.component('comp1').material('mat4').propertyGroup('KG').set('K', '');
model.component('comp1').material('mat4').propertyGroup('KG').set('G', '');
model.component('comp1').material('mat4').propertyGroup('KG').set('K', 'kappa(T[1/K])[Pa]');
model.component('comp1').material('mat4').propertyGroup('KG').set('INFO_PREFIX:K', ['Reference: M. Fukuhara, A. Sanpei, ISIJ International, v33, No. 4, p508 (1993) and J.A. Rayne, B.S. Chandrasekhar, Physical Review, v122, p1714 (1961)\nNote: approximate values for plain carbon and low alloy steels, values below 0C (273K) were calculated with C11, C12 and C44 from the average of Voigt and Reuss values and were increased by 6% to match the high temperature values']);
model.component('comp1').material('mat4').propertyGroup('KG').set('G', 'mu(T[1/K])[Pa]');
model.component('comp1').material('mat4').propertyGroup('KG').set('INFO_PREFIX:G', ['Reference: M. Fukuhara, A. Sanpei, ISIJ International, v33, No. 4, p508 (1993) and J.A. Rayne, B.S. Chandrasekhar, Physical Review, v122, p1714 (1961)\nNote: approximate values for plain carbon and low alloy steels, values below 0C (273K) were calculated from C11, C12, C44']);
model.component('comp1').material('mat4').propertyGroup('KG').addInput('temperature');
model.component('comp1').material('mat6').label('Germanium [solid,polycrystalline]');
model.component('comp1').material('mat6').set('family', 'custom');
model.component('comp1').material('mat6').set('customspecular', [0.7843137254901961 1 1]);
model.component('comp1').material('mat6').set('diffuse', 'white');
model.component('comp1').material('mat6').set('fresnel', 0.9);
model.component('comp1').material('mat6').set('roughness', 0.1);
model.component('comp1').material('mat6').set('metallic', 0);
model.component('comp1').material('mat6').set('pearl', 0);
model.component('comp1').material('mat6').set('diffusewrap', 0);
model.component('comp1').material('mat6').set('clearcoat', 0);
model.component('comp1').material('mat6').set('reflectance', 0);
model.component('comp1').material('mat6').propertyGroup('def').func('k_solid_1').label('Piecewise');
model.component('comp1').material('mat6').propertyGroup('def').func('k_solid_1').set('arg', 'T');
model.component('comp1').material('mat6').propertyGroup('def').func('k_solid_1').set('pieces', {'0.0' '1.0' '25.3*T^1+2.8*T^2';  ...
'1.0' '3.0' '-11.23649*T^1-0.4169014*T^2+51.7821*T^3-12.85493*T^4+0.8950217*T^5';  ...
'3.0' '25.0' '-1022.1+669.28*T^1-55.411*T^2+1.8941*T^3-0.024079*T^4';  ...
'25.0' '100.0' '3010.5-104.4*T^1+1.7061*T^2-0.013701*T^3+4.3023E-5*T^4';  ...
'100.0' '273.0' '1300.98-22.50806*T^1+0.1748131*T^2-6.917488E-4*T^3+1.36028E-6*T^4-1.054692E-9*T^5';  ...
'273.0' '1200.0' '194.4745-0.7376409*T^1+0.001233519*T^2-9.50629E-7*T^3+2.772653E-10*T^4'});
model.component('comp1').material('mat6').propertyGroup('def').func('alpha_solid_1').label('Piecewise 1');
model.component('comp1').material('mat6').propertyGroup('def').func('alpha_solid_1').set('arg', 'T');
model.component('comp1').material('mat6').propertyGroup('def').func('alpha_solid_1').set('pieces', {'40.0' '255.0' '2.65837E-6+2.823355E-8*T^1-1.091381E-10*T^2+1.67337E-13*T^3'; '255.0' '1085.0' '3.920731E-6+8.028266E-9*T^1-7.497433E-12*T^2+2.867304E-15*T^3'});
model.component('comp1').material('mat6').propertyGroup('def').func('C_solid_1').label('Piecewise 2');
model.component('comp1').material('mat6').propertyGroup('def').func('C_solid_1').set('arg', 'T');
model.component('comp1').material('mat6').propertyGroup('def').func('C_solid_1').set('pieces', {'100.0' '400.0' '-93.2985976+4.26282238*T^1-0.0172718867*T^2+3.24268242E-5*T^3-2.29865278E-8*T^4'; '400.0' '1211.0' '247.744347+0.379072157*T^1-5.4123178E-4*T^2+3.92362215E-7*T^3-1.0656217E-10*T^4'});
model.component('comp1').material('mat6').propertyGroup('def').func('HC_solid_1').label('Piecewise 3');
model.component('comp1').material('mat6').propertyGroup('def').func('HC_solid_1').set('arg', 'T');
model.component('comp1').material('mat6').propertyGroup('def').func('HC_solid_1').set('pieces', {'100.0' '400.0' '-6.77254457+0.309438264*T^1-0.00125376614*T^2+2.35386317E-6*T^3-1.66859217E-9*T^4'; '400.0' '1211.0' '17.9838236+0.02751685*T^1-3.92880069E-5*T^2+2.84815717E-8*T^3-7.73534573E-12*T^4'});
model.component('comp1').material('mat6').propertyGroup('def').func('VP_solid_1').label('Piecewise 4');
model.component('comp1').material('mat6').propertyGroup('def').func('VP_solid_1').set('arg', 'T');
model.component('comp1').material('mat6').propertyGroup('def').func('VP_solid_1').set('pieces', {'293.0' '1210.0' '(exp((-2.01500000e+04/T-9.10000000e-01*log10(T)+1.32800000e+01)*log(10.0)))*1.33320000e+02'});
model.component('comp1').material('mat6').propertyGroup('def').func('rho_solid_1').label('Piecewise 5');
model.component('comp1').material('mat6').propertyGroup('def').func('rho_solid_1').set('arg', 'T');
model.component('comp1').material('mat6').propertyGroup('def').func('rho_solid_1').set('pieces', {'40.0' '240.0' '5333.381+0.05517783*T^1-5.336078E-4*T^2+3.725521E-7*T^3+1.099984E-9*T^4'; '240.0' '1085.0' '5343.367-0.06555049*T^1-5.395979E-5*T^2+1.422598E-8*T^3'});
model.component('comp1').material('mat6').propertyGroup('def').func('TD_solid_1').label('Piecewise 6');
model.component('comp1').material('mat6').propertyGroup('def').func('TD_solid_1').set('arg', 'T');
model.component('comp1').material('mat6').propertyGroup('def').func('TD_solid_1').set('pieces', {'100.0' '179.0' '0.003686567-9.544598E-5*T^1+1.06306E-6*T^2-6.141435E-9*T^3+1.815428E-11*T^4-2.179196E-14*T^5'; '179.0' '473.0' '3.276892E-4-2.689072E-6*T^1+9.635243E-9*T^2-1.617223E-11*T^3+1.037439E-14*T^4'; '473.0' '1085.0' '1.145408E-4-4.405609E-7*T^1+7.362107E-10*T^2-5.661686E-13*T^3+1.647043E-16*T^4'});
model.component('comp1').material('mat6').propertyGroup('def').set('thermalconductivity', {'k_solid_1(T[1/K])[W/(m*K)]' '0' '0' '0' 'k_solid_1(T[1/K])[W/(m*K)]' '0' '0' '0' 'k_solid_1(T[1/K])[W/(m*K)]'});
model.component('comp1').material('mat6').propertyGroup('def').set('INFO_PREFIX:thermalconductivity', ['Reference: C.Y. Ho, R.W. Powell, P.E. Liley, Journal of Physical and Chemical Reference Data, v1, No. 2, p279 (1972) available online at https://srd.nist.gov/JPCRD/jpcrd7.pdf\nNote: well-annealed, high purity, error is 10% near 20C (293K), 25% below 20C (293K)']);
model.component('comp1').material('mat6').propertyGroup('def').set('thermalexpansioncoefficient', {'(alpha_solid_1(T[1/K])[1/K]+(Tempref-293[K])*if(abs(T-Tempref)>1e-3,(alpha_solid_1(T[1/K])[1/K]-alpha_solid_1(Tempref[1/K])[1/K])/(T-Tempref),d(alpha_solid_1(T[1/K])[1/K],T)))/(1+alpha_solid_1(Tempref[1/K])[1/K]*(Tempref-293[K]))' '0' '0' '0' '(alpha_solid_1(T[1/K])[1/K]+(Tempref-293[K])*if(abs(T-Tempref)>1e-3,(alpha_solid_1(T[1/K])[1/K]-alpha_solid_1(Tempref[1/K])[1/K])/(T-Tempref),d(alpha_solid_1(T[1/K])[1/K],T)))/(1+alpha_solid_1(Tempref[1/K])[1/K]*(Tempref-293[K]))' '0' '0' '0' '(alpha_solid_1(T[1/K])[1/K]+(Tempref-293[K])*if(abs(T-Tempref)>1e-3,(alpha_solid_1(T[1/K])[1/K]-alpha_solid_1(Tempref[1/K])[1/K])/(T-Tempref),d(alpha_solid_1(T[1/K])[1/K],T)))/(1+alpha_solid_1(Tempref[1/K])[1/K]*(Tempref-293[K]))'});
model.component('comp1').material('mat6').propertyGroup('def').set('INFO_PREFIX:thermalexpansioncoefficient', ['Reference: H.P. Singh, Acta Crystallographica, v24A, p469 (1968) and D.F. Gibbons, Physical Review, v112, p136 (1958)\nNote: the reference temperature is 20C (293K)\nReference temperature: 293.00[K]']);
model.component('comp1').material('mat6').propertyGroup('def').set('heatcapacity', 'C_solid_1(T[1/K])[J/(kg*K)]');
model.component('comp1').material('mat6').propertyGroup('def').set('INFO_PREFIX:heatcapacity', 'Reference: B.J. McBride, S. Gordon, M.A. Reno, NASA Technical Paper 3287 (1993)');
model.component('comp1').material('mat6').propertyGroup('def').set('HC', 'HC_solid_1(T[1/K])[J/(mol*K)]');
model.component('comp1').material('mat6').propertyGroup('def').set('INFO_PREFIX:HC', 'Reference: B.J. McBride, S. Gordon, M.A. Reno, NASA Technical Paper 3287 (1993)');
model.component('comp1').material('mat6').propertyGroup('def').set('VP', 'VP_solid_1(T[1/K])[Pa]');
model.component('comp1').material('mat6').propertyGroup('def').set('INFO_PREFIX:VP', 'Reference: Metallurgical Thermochemistry, 5th Edition, O. Kubasschewski and C.B. Alcock, Pergamon Press (1979)');
model.component('comp1').material('mat6').propertyGroup('def').set('density', 'rho_solid_1(T[1/K])[kg/m^3]');
model.component('comp1').material('mat6').propertyGroup('def').set('INFO_PREFIX:density', 'Reference: H.P. Singh, Acta Crystallographica, v24A, p469 (1968) and D.F. Gibbons, Physical Review, v112, p136 (1958)');
model.component('comp1').material('mat6').propertyGroup('def').set('TD', 'TD_solid_1(T[1/K])[m^2/s]');
model.component('comp1').material('mat6').propertyGroup('def').set('INFO_PREFIX:TD', 'Reference: calculated from the thermal conductivity, density and specific heat');
model.component('comp1').material('mat6').propertyGroup('def').set('lossfactor', '1e-5');
model.component('comp1').material('mat6').propertyGroup('def').addInput('temperature');
model.component('comp1').material('mat6').propertyGroup('def').addInput('strainreferencetemperature');
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').info('category').body('Solid_mechanics/');
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').func('dL_solid_1').label('Piecewise');
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').func('dL_solid_1').set('arg', 'T');
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').func('dL_solid_1').set('pieces', {'40.0' '230.0' '-9.807438E-4+3.298485E-6*T^1-8.591222E-8*T^2+9.459042E-10*T^3-3.739284E-12*T^4+5.243333E-15*T^5'; '230.0' '1085.0' '-0.00122932448+2.113466E-6*T^1+9.025808E-9*T^2-7.301805E-12*T^3+2.563101E-15*T^4'});
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').func('CTE_solid_1').label('Piecewise 1');
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').func('CTE_solid_1').set('arg', 'T');
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').func('CTE_solid_1').set('pieces', {'40.0' '110.0' '6.811591E-6-5.507621E-7*T^1+1.715101E-8*T^2-2.576646E-10*T^3+1.91419E-12*T^4-5.480769E-15*T^5'; '110.0' '300.0' '-2.175846E-5+5.300416E-7*T^1-4.420334E-9*T^2+1.880642E-11*T^3-3.989769E-14*T^4+3.36312E-17*T^5'});
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').set('alphatan', '');
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').set('dL', '');
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').set('alphatan', {'CTE_solid_1(T[1/K])[1/K]' '0' '0' '0' 'CTE_solid_1(T[1/K])[1/K]' '0' '0' '0' 'CTE_solid_1(T[1/K])[1/K]'});
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').set('INFO_PREFIX:alphatan', 'Reference: D.F. Gibbons, Physical Review, v112, p136 (1958)');
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').set('dL', {'(dL_solid_1(T[1/K])-dL_solid_1(Tempref[1/K]))/(1+dL_solid_1(Tempref[1/K]))' '0' '0' '0' '(dL_solid_1(T[1/K])-dL_solid_1(Tempref[1/K]))/(1+dL_solid_1(Tempref[1/K]))' '0' '0' '0' '(dL_solid_1(T[1/K])-dL_solid_1(Tempref[1/K]))/(1+dL_solid_1(Tempref[1/K]))'});
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').set('INFO_PREFIX:dL', ['Reference: H.P. Singh, Acta Crystallographica, v24A, p469 (1968) and D.F. Gibbons, Physical Review, v112, p136 (1958)\nNote: the reference temperature is 20C (293K)\nReference temperature: 293.00[K]']);
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').addInput('temperature');
model.component('comp1').material('mat6').propertyGroup('ThermalExpansion').addInput('strainreferencetemperature');
model.component('comp1').material('mat6').propertyGroup('Enu').func('E_solid_polycrystalline_1').label('Piecewise 2');
model.component('comp1').material('mat6').propertyGroup('Enu').func('E_solid_polycrystalline_1').set('arg', 'T');
model.component('comp1').material('mat6').propertyGroup('Enu').func('E_solid_polycrystalline_1').set('pieces', {'73.0' '573.0' '1.354753E11-9935235.0*T^1-5585.968*T^2'});
model.component('comp1').material('mat6').propertyGroup('Enu').func('nu').label('Piecewise 1');
model.component('comp1').material('mat6').propertyGroup('Enu').func('nu').set('arg', 'T');
model.component('comp1').material('mat6').propertyGroup('Enu').func('nu').set('pieces', {'73.0' '573.0' '0.2083703-8.544554E-6*T^1+1.459159E-8*T^2'});
model.component('comp1').material('mat6').propertyGroup('Enu').set('E', '');
model.component('comp1').material('mat6').propertyGroup('Enu').set('nu', '');
model.component('comp1').material('mat6').propertyGroup('Enu').set('E', 'scale_Ge_E*E_solid_polycrystalline_1(T[1/K])[Pa]');
model.component('comp1').material('mat6').propertyGroup('Enu').set('nu', 'scale_Ge_nu*nu(T[1/K])');
model.component('comp1').material('mat6').propertyGroup('Enu').addInput('temperature');
model.component('comp1').material('mat6').propertyGroup('KG').func('mu').set('arg', 'T');
model.component('comp1').material('mat6').propertyGroup('KG').func('mu').set('pieces', {'73.0' '573.0' '5.605833E10-3730424.0*T^1-2966.808*T^2'});
model.component('comp1').material('mat6').propertyGroup('KG').func('kappa').label('Piecewise 1');
model.component('comp1').material('mat6').propertyGroup('KG').func('kappa').set('arg', 'T');
model.component('comp1').material('mat6').propertyGroup('KG').func('kappa').set('pieces', {'73.0' '573.0' '7.741619E10-7840838.0*T^1+533.0671*T^2'});
model.component('comp1').material('mat6').propertyGroup('KG').set('K', '');
model.component('comp1').material('mat6').propertyGroup('KG').set('G', '');
model.component('comp1').material('mat6').propertyGroup('KG').set('K', 'kappa(T[1/K])[Pa]');
model.component('comp1').material('mat6').propertyGroup('KG').set('INFO_PREFIX:K', ['Reference: H.J. McSkimin, Journal of the Acoustical Society of America, v31, p287 (1959)\nNote: calculated from E and mu, errors may be large']);
model.component('comp1').material('mat6').propertyGroup('KG').set('G', 'mu(T[1/K])[Pa]');
model.component('comp1').material('mat6').propertyGroup('KG').set('INFO_PREFIX:G', 'Reference: H.J. McSkimin, Journal of the Acoustical Society of America, v31, p287 (1959)');
model.component('comp1').material('mat6').propertyGroup('KG').addInput('temperature');

model.common('cminpt').set('modified', {'temperature' '123[K]'});

model.component('comp1').physics('solid').feature('dcnt1').set('pairDisconnect', true);
model.component('comp1').physics('solid').feature('dcnt1').label('Contact');
model.component('comp1').physics('solid').feature('dcont1').set('pairDisconnect', true);
model.component('comp1').physics('solid').feature('dcont1').label('Continuity');
model.component('comp1').physics('sens').active(false);
model.component('comp1').physics('sens').feature('gobj1').set('objectiveExpression', 'solid.Q_eig');
model.component('comp1').physics('sens').feature('gcvar1').set('variableList', {'thickness'; 'coating'});
model.component('comp1').physics('sens').feature('gcvar1').set('initList', {'100[um]'; '500[nm]'});
model.component('comp1').physics('sens').feature('gcvar1').set('valueType', 'real');
model.component('comp1').physics('sens2').active(false);
model.component('comp1').physics('sens2').feature('iobj1').set('objectiveExpression', 'solid.Ws');
model.component('comp1').physics('sens2').feature('gcvar1').set('variableList', {'thickness'; 'coating'});
model.component('comp1').physics('sens2').feature('gcvar1').set('initList', {'100[um]'; '500[nm]'});
model.component('comp1').physics('sens2').feature('gcvar1').set('valueType', 'real');

model.component('comp1').mesh('mesh1').feature('size').set('hauto', 1);
model.component('comp1').mesh('mesh1').feature('ftri1').active(false);
model.component('comp1').mesh('mesh1').feature('swe1').set('sweeppath', 'general');
model.component('comp1').mesh('mesh1').feature('swe1').set('facemethod', 'tri');
model.component('comp1').mesh('mesh1').feature('swe1').selection('sourceface').named('geom1_wp1_bnd');
model.component('comp1').mesh('mesh1').feature('swe1').selection('targetface').named('geom1_boxsel2');
model.component('comp1').mesh('mesh1').feature('swe2').set('facemethod', 'tri');
model.component('comp1').mesh('mesh1').feature('swe2').selection('sourceface').named('geom1_boxsel1');
model.component('comp1').mesh('mesh1').feature('swe2').selection('targetface').named('geom1_boxsel3');
model.component('comp1').mesh('mesh1').feature('swe2').feature('dis1').set('numelem', 3);
model.component('comp1').mesh('mesh1').feature('fq1').active(false);
model.component('comp1').mesh('mesh1').feature('cpf1').active(false);
model.component('comp1').mesh('mesh1').feature('cpf1').selection('source').set([6 9]);
model.component('comp1').mesh('mesh1').feature('cpf1').selection('destination').set([6 9]);
model.component('comp1').mesh('mesh1').feature('ftet1').feature('size1').set('hauto', 7);
model.component('comp1').mesh('mesh1').feature('ftet2').set('smoothmaxiter', 10);
model.component('comp1').mesh('mesh1').feature('ftet2').set('smoothmaxdepth', 10);
model.component('comp1').mesh('mesh1').feature('ftet2').set('optlevel', 'high');
model.component('comp1').mesh('mesh1').feature('ftet2').set('optlarge', true);
model.component('comp1').mesh('mesh1').feature('ftet2').feature('size1').set('hauto', 7);
model.component('comp1').mesh('mesh1').run;

model.study.create('std1');
model.study('std1').create('eig', 'Eigenfrequency');

model.sol.create('sol1');
model.sol('sol1').study('std1');
model.sol('sol1').attach('std1');
model.sol('sol1').create('st1', 'StudyStep');
model.sol('sol1').create('v1', 'Variables');
model.sol('sol1').create('e1', 'Eigenvalue');

model.result.numerical.create('int1', 'IntVolume');
model.result.numerical.create('int2', 'IntVolume');
model.result.numerical('int1').selection.named('geom1_ext2_dom');
model.result.numerical('int1').set('probetag', 'none');
model.result.numerical('int2').selection.named('geom1_csel1_dom');
model.result.numerical('int2').set('probetag', 'none');
model.result.create('pg1', 'PlotGroup3D');
model.result('pg1').create('surf1', 'Surface');
model.result('pg1').feature('surf1').create('def', 'Deform');

model.study('std1').feature('eig').set('neigs', 20);
model.study('std1').feature('eig').set('neigsactive', true);
model.study('std1').feature('eig').set('shift', '1000');
model.study('std1').setStoreSolution(true);

model.sol('sol1').attach('std1');
model.sol('sol1').feature('st1').label('Compile Equations: Eigenfrequency');
model.sol('sol1').feature('v1').label('Dependent Variables 1.1');
model.sol('sol1').feature('e1').label('Eigenvalue Solver 1.1');
model.sol('sol1').feature('e1').set('transform', 'eigenfrequency');
model.sol('sol1').feature('e1').set('neigs', 20);
model.sol('sol1').feature('e1').set('shift', '1000');
model.sol('sol1').feature('e1').set('eigvfunscale', 'maximum');
model.sol('sol1').feature('e1').set('eigvfunscaleparam', 3.1399999999999997E-8);
model.sol('sol1').feature('e1').feature('dDef').label('Direct 1');
model.sol('sol1').feature('e1').feature('aDef').label('Advanced 1');
model.sol('sol1').feature('e1').feature('aDef').set('cachepattern', true);
model.sol('sol1').runAll;

model.result.numerical('int1').label('coating SER');
model.result.numerical('int1').set('table', 'tbl1');
model.result.numerical('int1').set('expr', {'solid.Ws / solid.Ws_tot'});
model.result.numerical('int1').set('unit', {'1'});
model.result.numerical('int1').set('descr', {''});
model.result.numerical('int1').set('const', {'solid.refpntx' '0' 'Reference point for moment computation, x coordinate'; 'solid.refpnty' '0' 'Reference point for moment computation, y coordinate'; 'solid.refpntz' '0' 'Reference point for moment computation, z coordinate'});
model.result.numerical('int2').label('clamp SER');
model.result.numerical('int2').set('table', 'tbl2');
model.result.numerical('int2').set('expr', {'solid.Ws / solid.Ws_tot'});
model.result.numerical('int2').set('unit', {'1'});
model.result.numerical('int2').set('descr', {''});
model.result.numerical('int2').set('const', {'solid.refpntx' '0' 'Reference point for moment computation, x coordinate'; 'solid.refpnty' '0' 'Reference point for moment computation, y coordinate'; 'solid.refpntz' '0' 'Reference point for moment computation, z coordinate'});
model.result.numerical('int1').setResult;
model.result.numerical('int2').setResult;
model.result.evaluationGroup('std1EvgFrq').active(false);
model.result.evaluationGroup('std1EvgFrq').label('Eigenfrequencies (Study 1)');
model.result.evaluationGroup('std1EvgFrq').set('data', 'dset1');
model.result.evaluationGroup('std1EvgFrq').set('looplevelinput', {'manual'});
model.result.evaluationGroup('std1EvgFrq').set('looplevel', {'1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20'});
model.result.evaluationGroup('std1mpf1').active(false);
model.result.evaluationGroup('std1mpf1').label('Participation Factors (Study 1)');
model.result.evaluationGroup('std1mpf1').set('data', 'dset1');
model.result.evaluationGroup('std1mpf1').set('looplevelinput', {'manual'});
model.result.evaluationGroup('std1mpf1').set('looplevel', {'1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20'});
model.result('pg1').label('Mode Shape (solid)');
model.result('pg1').set('showlegends', false);
model.result('pg1').feature('surf1').set('const', {'solid.refpntx' '0' 'Reference point for moment computation, x coordinate'; 'solid.refpnty' '0' 'Reference point for moment computation, y coordinate'; 'solid.refpntz' '0' 'Reference point for moment computation, z coordinate'});
model.result('pg1').feature('surf1').set('colortable', 'AuroraBorealis');
model.result('pg1').feature('surf1').set('threshold', 'manual');
model.result('pg1').feature('surf1').set('thresholdvalue', 0.2);
model.result('pg1').feature('surf1').set('resolution', 'normal');
model.result('pg1').feature('surf1').feature('def').set('scale', 51061.37024707783);
model.result('pg1').feature('surf1').feature('def').set('scaleactive', false);

out = model;
