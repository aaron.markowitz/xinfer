## The mechanical system

I reduced the trident-of-tridents model to just a single trident described by 11 parameters, defined in trident.m
- `h_left`, `h_middle`, `h_right`, the heights of the left, middle and right spokes of the trident respectively.
- `h_leg`, the height of the leg between the trident box and clamped base
- `h_box`, the height of the trident box
- `fin_space_l` the gap between the left and middle tridents
- `fin_space_r` the gap between the middle and right tridents
- `w_left`, the width of the left trident
- `w_mid`, the width of the middle trident
- `w_right`, the width of the right trident
- `h_coat`, the y-extent of the coating from the top of the trident

## How to adapt this script

Here are some tips on likely near-term directions

### Base COMSOL model

You may want to update something about the COMSOL model, like the geometric parameterization. 

1. Design the model you want, compact its history, and export as a .m file in the models folder
2. Add the line `model.hist.disable;` just after the model is defined in the .m file. This will prevent the model from using more memory as it is repeatedly updated during optimization.
3. Change the path in `model.modelPath` to the location of your model.
4. In xopt, replace `eng.trident()` with a call to your new model.
5. Update or replace `run_trident.m` for your new model. This function checks that certain conditions are true before running the model -- for example, to avoid negative length parameters. If your COMSOL model fails to run, the model may no longer be properly defined for future steps in the optimization.

You may want to update the base model and `run_trident.m` to avoid narrow features that result in extremely fine meshes. For example, if h_coat puts the coating close to but not past a horizontal (x-axis parallel) edge of the substrate, this will result in a narrow feature where the coating ends.  I have noticed that the optimization tends to seek out these narrow features, which could indicate unwanted mesh-dependence in the design.

### hyperparameters

You can change the following hyperparameters in xopt.py in the dictionary `hp`
- `run_name` is the name to prepend to output files
- `params` lets you choose which model parameters to optimize
- `bounds` gives the bounds of these model parameters
- `phi_clamp`, `phi_coat`, and `phi_sub` are samples from the prior distribution of the mechanical loss angles. Since I just used logspace, they are log-uniform priors. The utility function is averaged over a meshgrid of these prior values.
- `sig_lPhi` is the proposed variance of the log(1/Q) measurements.
- The various `f0_` and `alpha_` are used to define the power law frequency scaling of mechanical loss angles. For example, $\phi_\mathrm{substrate}(f) = \phi_\mathrm{substrate}(f_{0,\mathrm{substrate}}) * (\frac{f}{f_{0,\mathrm{substrate}}})^{\alpha_\mathrm{substrate}}$. I gave the substrate an $f$ scaling like Akhiezer loss, and gave the clamp and coating both flat $f^0$ scaling.

### Inference over multiple parameters

To infer over all parameters in $\theta$, take the trace of the Fisher information, as in the commented code in U_FIG
`U = np.einsum('iij->j', F, optimize='optimal')`

To infer over more than one parameter but not all parameters, one can invert F, then pull out the sub-matrix of $F^{-1}$ corresponding to the parameters of interest, then invert once more, then take the trace.

### Inference over power laws

Adding new elements of $\theta$ that do not obey the simple regression relation of the loss angles $\phi$ requires explicitly defining the derivatives of $\Sigma$ and $\vec{\mu}$ with respect to the new parameters. This can be done in `ddtheta` and `dSigma`, but care should be taken that the new functions are compatible with `fisher`. For example, you might want to infer the power law scaling $\alpha_\mathrm{coat}$. 

Any new elements of $\vec{\theta}$ should also be given their own prior distributions, and `compute_cost` should be updated to include the new parameters in the prior sampling meshgrid.
