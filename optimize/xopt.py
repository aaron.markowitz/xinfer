import numpy as np
import emcee
from numpy.random import randn, rand
import scipy
from scipy.stats import norm

import pickle
#from schwimmbad import MPIPool
import time

import sys
from helpers import *

import matlab.engine



eng = matlab.engine.start_matlab()
eng.addpath('../src', nargout=0)

eng.connect_comsol(nargout=0)
#model = eng.oneSided_Ge_paramd()
model = eng.trident()

# hyperparameters
hp = {
    'run_name': 'trident_da1', # prepended to filenames
    #'argmap': ['argmap', 'params', 'model', 'eng',
    #           'phi_clamp', 'phi_coat', 'phi_sub',
    #           'y_samp', 'sig_loss', 'Nsamples'
    #       ],
    # experimental parameters to optimize
    # these params were used with oneSided_Ge_paramd model
    #'params': ['h_left', 'h_middle', 'h_right',
    #           'h_leg', 'h_box',
    #           'w_gap', 'w_leg', 'fin_space',
    #           'outfin', 'midfin'
    #       ],
    'params': [
        'h_left', 'h_middle', 'h_right',
        'h_leg', 'h_box',
        'fin_space_l', 'fin_space_r',
        'w_left', 'w_mid', 'w_right',
        'h_coat'
    ],
    # parameters to help estimate our information gains
    #'nuisance': ['dtheta_clamp', 'dtheta_coat'], #, 'dtheta_sub']
    'model': model,
    'eng': eng,
    'bounds': [
        (5, 20), # left leg height in mm
        (5, 20), # middle leg height in mm
        (5, 20),  # right leg height in mm
        (0.5, 5), # trident leg height in mm
        (1, 10), # trident box height in mm
        #(0.25, 2), # trident gap width in mm
        (0.25, 5), # trident left fin space in mm
        (0.25, 5), # trident right fin space in mm
        #(1, 3), # trident leg width in mm
        #(0.25, 2), # trident fin space in mm
        (0.5, 5), # trident left fin width in mm
        (0.5, 5), # trident middle fin width in mm
        (0.5, 5), # trident right fin width in mm
        (1, 25) # height of coating region from top of resonator, in mm
    ],
    #'phi_clamp': 1,
    #'phi_coat': 1e-5,
    #'phi_sub': 1e-8,
    'phi_clamp': np.logspace(-2, 0, 11),
    'phi_coat': np.logspace(-6, -4, 11),
    # if phi_coat approaches phi_sub, we should also samples from our prior on phi_sub
    # since our uncertainty on phi_sub would become a systematic uncertainty on phi_coat
    'phi_sub': np.logspace(-9, -7, 11),
    #'y_samp': np.linspace(0.95, 1.05, 11), #np.logspace(-9, 0, 1000),
    #'sig_loss': .01,
    'sig_lPhi': .05,
    'f0_sub' : 3e3, # Hz, normalization frequency for substrate (Akhiezer) loss \propto f^alpha_sub
    'f0_coat' : 3e3, # Hz, normalization frequency for coating loss \propto f^alpha_coat
    'f0_clamp' : 3e3, # Hz, normalization frequency for clamp loss \propto f^alpha_clamp
    'alpha_coat' : 0, #np.linspace(-2, 2, 11), # prior on power law for coating loss
    'alpha_sub' : 1, # power law for substrate loss
    'alpha_clamp' : 0 # power law for clamp loss
    #'Nqmc': int(2**4),
    #'Ncon': 4 # number of contrastive samples for information gain estimate
}

#args = ()
#for arg in hp['argmap']:
#    args = args + (hp[arg],)

#print(args)

#global cost_min
#cost_min = 0

# this x0 used for oneSided_Ge_paramd model
#x0 = [18.30229741 14.0615     15.62836489  4.84160557  2.10091204  0.40293646
#  1.03106977  0.31014322  2.79451607  2.21776361]

# this x0 used for trident model
#x0 = [15, 15, 15, 3, 5, .5, .5, 1, 3, 1, 20]
# needed to stop simulation to modify trident model, here is a local minimum
#x0 = [ 9.9286572,  19.9616454,  16.14342677,  0.51988291,  4.30834107,  1.92676586,
#  0.77742054,  0.94480912,  1.34457567,  0.53541852, 16.64925647]
#x0 = [18.76736776, 11.35610862,  5.58900493,  1.26313934,  1.87979992,  2.88574684,
#  0.91358738,  1.28290278,  1.09145129,  1.50517206, 21.92858131]
#x0 = [15, 17, 15, 3, 4, .5, .5, 3, 5, 3, 20]
x0 = [13.56672049, 13.71571601, 12.82694249,  2.23046935, 7.43690805,  2.02770528,
  2.7040486,   1.81772612,  1.71577467,  4.89275608, 23.39319484]

#res = scipy.optimize.shgo(compute_cost, bounds=hp['bounds'], args=(hp,), iters=2,
#                              options={'disp': True}
#)
res = scipy.optimize.dual_annealing(compute_cost, bounds=hp['bounds'], args=(hp,), x0=x0
)

with open(hp['run_name'] + '_res.pkl', 'wb') as f:
    pickle.dump(res, f)

eng.ModelUtil.disconnect(nargout=0)
