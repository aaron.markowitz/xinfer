function connect_comsol()
  
addpath('/usr/local/comsol60/multiphysics/mli/')
mphstart('sandbox2.ligo.caltech.edu', 2020, 'whoever', 'whatever');

import com.comsol.model.*;
import com.comsol.model.util.*;

%model = oneSide_Ge_coarse;

%params = ["thickness", "coating"];
%x = ["100[um]", "500[nm]"];

%[freqs, SEsub, SEcoat, SEclamp] = run_experiment(x, params, model);

%x0 = [100, 500];
%N = 10;
%smpl = mhsample(x0, 'pdf', post, 'proppdf');
