import numpy as np
from numpy.random import randn, rand
from scipy.stats import norm
from scipy.stats import reciprocal
from scipy import stats
#import matlab.engine

global cost_min
global x_best
cost_min = np.inf
x_best = []

# frequencies and Qs given parameters
def run_experiment(params, model=None, eng=None):

    px, pparams = parseParams(params)

    # print(px)
    # print(pparams)
    # print(model)
    # print(eng)
    # print('all variables present')
    
    freqs, SEsub, SEcoat, SEclamp = eng.run_experiment(px, pparams, model, nargout=4)
    # print(freqs)
    # print(SEsub)
    # print(SEcoat)
    # print(SEclamp)
    SEtot = np.array(SEsub) + np.array(SEcoat) + np.array(SEclamp)
    SERsub = np.array(SEsub) / SEtot
    SERcoat = np.array(SEcoat) / SEtot
    SERclamp = np.array(SEclamp) / SEtot

    # print(SERsub)
    # print(SERcoat)
    # print(SERclamp)
    
    phi_tots = (10**params['lphi_sub'] * SERsub +
                10**params['lphi_coat'] * SERcoat +
                10**params['lphi_clamp'] * SERclamp)
    Qs = 1 / phi_tots

    # print(Qs)

    ff = np.reshape(np.array(freqs), np.shape(Qs[0]))
    QQ = np.log10(Qs[0])

    return np.array([ff, QQ])

def comsol_SERs(x, params_list, nuisance_list=None, hp={}):
    """
    Compute the integrated strain energies for each eigenmode and subdomain using COMSOL model.

    Args:
        x (array-like): The input parameters.
        params_list (list): The list of parameter names.
        nuisance_list (list, optional): The list of nuisance parameters. Defaults to None.
        hp (dict, optional): Additional parameters for the COMSOL model. Defaults to {}.

    Returns:
        tuple: A tuple containing the frequency, strain energy ratios for the substrate, coat, and clamp.

    Raises:
        Exception: If the COMSOL model fails to run.
    """

    # don't feed comsol our nuisance parameters
    if nuisance_list == None:
        xx = x
    else:
        xx = x[:-len(hp['nuisance'])]

    #print(params_list)
    #print(xx)
    px, pparams = parseParams(dict(zip(params_list, xx)))

    #print(hp['model'])
    #print(px)
    #print(pparams)
    # compute comsol model to get integrated strain energies for each eigenmode and subdomain
    try:
        #test = eng.run_experiment(px, pparams, hp['model'], nargout=4)
        #print(test)
        freqs, SEsub, SEcoat, SEclamp = hp['eng'].run_trident( #hp['eng'].run_experiment(
            px, pparams, hp['model'], nargout=4
        )
    except:
        print('failed to run model at ')
        print(x)
        print('optimum remains ' + str(cost_min) + ' at ' + str(x_best))
        return np.inf

    # strain energy ratios are fractions of total strain energy
    SEtot = np.array(SEsub) + np.array(SEcoat) + np.array(SEclamp)
    SERsub = np.array(SEsub) / SEtot
    SERcoat = np.array(SEcoat) / SEtot
    SERclamp = np.array(SEclamp) / SEtot

    return np.array(freqs).flatten(), SERsub.flatten(), SERcoat.flatten(), SERclamp.flatten()

def _Isamp(p_y_theta0, p_y_thetal, p_thetal, q_thetal_y, Ncon):
    # NOT IMPLEMENTED
    # sample from distribution of i_ACE over theta0, y, theta_1:L
    # inputs:
    #   p_y_theta0 : pdf of y | theta evaluated at y_samp and theta0
    #                scalar
    #   p_y_thetal : pdf of y | theta evaluated at y_samp and thetal
    #                (Ncon+1,)
    #   p_thetal : prior likelihood evaluated at thetal
    #                (Ncon+1,)
    #   q_thetal_y : candidate posterior evaluated at thetal | y
    #                (Ncon+1,)
    #   Ncon : number of contrastive samples
    #          scalar
    # returns:
    #   one sample of I_ACE used for evaluating expectation
    #   scalar
    denom = np.sum(p_thetal * p_y_thetal / q_thetal_y) / (Ncon + 1)
    return np.log10(p_y_theta0 / denom)


def _E_thetal(theta0, Ncon):
    # NOT IMPLEMENTED
    # calculate expectation of I_ACE|theta0,y over theta_1:L
    # inputs:
    #   theta0 : nominal value of inferred parameters
    #            (Ndom,)
    #   Ncon : number of contrastive samples
    #          scalar
    # returns:
    
    # generate QMC samples of theta_1L
    #means = np.broadcast_to(theta0, np.append(Ncon, 
    #    
        

    # calculate Isamp for each sample of theta_1L

    # integrate
    #exp = np.sum(q_theta1L_y * Isamp_1L

    return exp

def _Iace(xi, phi, Ncon):
    # NOT IMPLEMENTED
    # compute expected information gain from adaptive contrastive estimation
    # inputs:
    #   xi : array of experimental parameters being optimized
    #        in this case strain energy ratios of Ndom material domains at Neig eigenmodes
    #        shape(xi) = (Neig, Ndom)
    #   phi : array of nuisance parameters of the posterior function, also optimized over
    #         in this case, standard deviations of the posterior on inferred loss angles theta
    #         shape(phi) = (Nnuisance,)
    #   Ncon : number of contrastive samples to use in estimating Iace
    #          scalar
    # returns: scalar estimate of Iace

    # generate 
    #p_theta0
    return

def _compute_Ipce(x, *args):
    # NOT IMPLEMENTED
    # I prefer dicts to lists for arguments
    hp = args[0]

    #Sshape = np.ones(2+hp['Ncon'], dtype=int) * hp['Nqmc']
    #Samps = np.ones(Sshape)

    # comsol predicts strain energy ratios from experimental parameters
    if len(x) == len(hp['params']):
        #if the outer optimizer is not working over nuisance parameters
        freqs, SERsub, SERcoat, SERclamp = comsol_SERs(x, hp['params'], hp=hp)
    else:
        freqs, SERsub, SERcoat, SERclamp = comsol_SERs(x, hp['params'],
                                                       hp['nuisance'], hp=hp)

    # our many experimental parameters can map onto strain energy ratios
    # for each of the material domains
    xi = np.reshape(np.array([SERsub, SERcoat, SERclamp]), (3, len(SERsub)))

    # theta0 are sampled from our prior on the inferred parameters
    # in this case, material loss angles
    theta0 = np.array(np.meshgrid(phi_sub, phi_coat, phi_clamp, indexing='ij'))

    # for each value of theta0, we draw Nsamples contrastive samples used to estimate the information gain
    # the contrastive samples are drawn from a candidate posterior distribution q_phi(thetal|y)
    # for large Nsamples, q_phi doesn't need to be a perfect approximation of p(thetal|y), so we use a simpler model
    #thetal = np.broadcast_to(theta0, np.append(Ncon

    
    y_nom = np.einsum('ijkl,im->mjkl', theta, xi)

    y_nom_tiled = np.broadcast_to(y_nom, np.append(len(y_samp), np.shape(y_nom)))
    y_samp_tiled = np.swapaxes(np.swapaxes(y_nom_tiled, 0, -1) * y_samp, -1, 0)
    
    likelihoods = np.prod(
        norm.pdf(y_samp_tiled, y_nom_tiled, y_nom_tiled * sig_loss),
        axis = 1
    )

    denom = np.einsum('ijkl->i', likelihoods) / np.prod(np.shape(theta[0]))
    denom_tiled = np.broadcast_to(
        np.reshape(denom, np.append(len(denom), np.ones(len(np.shape(theta[0])), dtype=int))),
        np.append(len(denom), np.shape(theta[0]))
    )
    Isamp = np.log10(np.divide(likelihoods, denom_tiled))

    prior = np.ones(np.shape(theta[0])) / np.prod(np.shape(theta[0]))

    IPCE = np.einsum('ijkl,jkl,ijkl,jkl', Isamp, prior, likelihoods, prior)

    return IPCE

def ddtheta(invQ, xi):
    """
    Compute the derivative of u with respect to theta for each domain
    du_dtheta_ijk = e^(-u_ik) * xi_ij = Q_ik * xi_ij
    inputs:
    invQ : array of 1/Q values
        shape(invQ) = (N_eig, N_prior)
    xi : array of experimental parameters
        shape(xi) = (N_dom, N_eig)
    """

    invQ_mat = np.broadcast_to(invQ, np.append(np.shape(xi), np.shape(invQ)[1]))
    xi_mat = np.broadcast_to(
        np.reshape(xi, np.append(np.shape(xi), 1)),
        np.append(np.shape(xi), np.shape(invQ)[1])
        )

    return xi_mat / invQ_mat

def invSigma(uu, hp={}):
    """
    Compute the 1/covariance matrix of the log(1/Q) observations
    inputs:
    uu : array of log(1/Q) values
        shape(uu) = (N_eig, N_prior)
    N_dom : number of material domains
        scalar
    returns:
    cov : array of covariance matrices
        shape(cov) = (N_eig, N_eig, N_prior)
    """

    invcov_vec = 1/(hp['sig_lPhi'] * uu)**2
    N_eig = np.shape(uu)[0]

    invCov = np.broadcast_to(
        np.transpose([np.identity(N_eig)]), (N_eig, N_eig, np.shape(uu)[1])
        ) * np.broadcast_to(invcov_vec, (N_eig, N_eig, np.shape(uu)[1])
    )

    return invCov

def dSigma(uu, du_dtheta, hp={}):
    """
    Calculate derivative of covariance matrix wrt theta
    inputs:
    uu : log(1/Q) values
        shape(uu) = (N_eig, N_prior)
    du_dtheta : derivative of u with respect to theta
        shape(dudtheta) = (N_dom, N_eig, N_prior)
    returns:
    dCovdtheta : derivative of covariance matrix with respect to theta
        shape(dCovdtheta) = (N_dom, N_eig, N_eig, N_prior)
    """

    dSig = 2 * hp['sig_lPhi']**2 * np.einsum(
        'dak,bk,ab->dabk',
        du_dtheta, uu, np.identity(np.shape(uu)[0]), optimize='optimal'
    )
    return dSig

def fisher(xi, thetas, hp={}):
    """
    Compute the Fisher information matrix of measured log(1/Q) at parameter values theta
    xi : array of experimental parameters
            shape(xi) = (N_dom, N_eig)
    thetas : array of inferred parameters
            shape(theta) = (N_dom, N_prior)
    hp : dict of hyperparameters
        hp['theta'] : array of N_prior inferred parameters drawn from their prior
            shape(theta) = (N_dom, N_prior)
    """

    # compute 1/Q for mechanical modes
    # invQ has shape (N_eig, N_prior)
    invQ = np.einsum('ij,ik->jk', xi, thetas)

    # uu is log(1/Q), and is a normal random variable
    # with shape (N_eig, N_prior)
    uu = np.log(invQ)

    # compute the derivative of u with respect to theta for each domain
    # with shape (N_dom, N_eig, N_prior)
    # I multiplied by thetas so the derivative becomes wrt log(theta), 
    # since realistic priors on thetas are log-normal
    # (simple reparametrization)
    du_dtheta = np.einsum('ijk,ik->ijk', ddtheta(invQ, xi), thetas, optimize='optimal')

    # covariance matrix of the log(1/Q) observations
    # cov has shape (N_eig, N_eig, N_prior)
    invCov = invSigma(uu, hp=hp)

    # derivative of covariance matrix wrt theta
    # shape(dSigma_dtheta) = (N_dom, N_eig, N_eig, N_prior)
    dSigma_dtheta = dSigma(uu, du_dtheta, hp=hp)

    F = (
        np.einsum(
            'mik,ijk,njk->mnk', du_dtheta, invCov, du_dtheta, optimize='optimal'
        ) +
        np.einsum( # trace over matrix product
            'iimnk->mnk', np.einsum( # matrix product, at each mn and theta
                'iak,mabk,bck,ncjk->ijmnk', 
                invCov, dSigma_dtheta, invCov, dSigma_dtheta, optimize='optimal'
            ), optimize='optimal'
        )
    )

    return F

def U_FIG(xi, thetas, hp={}):
    """
    Compute the expected Fisher information gain for the parameters theta at experimental design xi
    thetas : array of inferred parameters
            shape(theta) = (N_dom, N_prior)
    hp : dict of hyperparameters
            hp['theta'] : array of N_prior inferred parameters drawn from their prior
    xi : array of experimental parameters
            shape(xi) = (N_dom, N_eig)
    returns: Fisher information matrix
    """
    # compute Fisher matrix
    # F has shape (N_dom, N_dom, N_prior)
    F = fisher(xi, thetas, hp=hp)

    # trace of fisher matrix at each theta
    # N_prior dimensional array
    # After normalizing over prior, this is a fully Bayesian utility function for EIG
    # See Walker 2016, "Bayesian information in an experiment and the Fisher information distance"
    # U = #trF = np.einsum('iij->j', F, optimize='optimal')

    # Because we don't actually care about estimating phi_sub and phi_clamp, we can 
    # invert F and pick out the MLE estimate for phi_coat
    # note this only asymptotically valid for many observations, ie small uncertainty hp['sig_lPhi']
    MLE = np.einsum(
        'kij->ijk', # return to original indices after following np broadcasting rules
        np.linalg.inv(
            np.einsum('ijk->kij', F) # dimensions to invert over must be last
        )
    )
    U = 1/MLE[1,1] # 1/variance of MLE for phi_coat

    return np.mean(U)

def f_weights(freqs, hp={}):
    """
    Create an array of frequency dependent weights for each subdomain
    freqs : array of eigenfrequencies
            shape(freqs) = (N_eig,)
    hp : dict of hyperparameters
    return f_f0 : array of frequency dependent weights
            shape(f_f0) = (N_dom, N_eig)
    """

    sub_w = (freqs / hp['f0_sub'])**hp['alpha_sub']
    coat_w = (freqs / hp['f0_coat'])**hp['alpha_coat']
    clamp_w = (freqs / hp['f0_clamp'])**hp['alpha_clamp']

    f_f0 = np.array([sub_w, coat_w, clamp_w])

    return f_f0

def compute_cost(x, *args):
    hp = args[0]
    #print(hp)
    #print(hp['model'])

    try:
        # comsol predicts strain energy ratios from experimental parameters
        if len(x) == len(hp['params']):
            #if the outer optimizer is not working over nuisance parameters
            #print('no nuisance params')
            freqs, SERsub, SERcoat, SERclamp = comsol_SERs(x, hp['params'], hp=hp)
        else:
            #print('with nuisance params')
            freqs, SERsub, SERcoat, SERclamp = comsol_SERs(x, hp['params'],
                                                           hp['nuisance'], hp=hp)
    except:
        return np.inf

    print('successfully computed model')

    # our many experimental parameters can map onto strain energy ratios
    # for each of the material domains
    #print(SERsub)
    #print(SERcoat)
    #print(SERclamp)
    #print(np.shape(SERsub))
    #print(np.shape(SERcoat))
    #print(np.shape(SERclamp))

    SERs = np.array([SERsub, SERcoat, SERclamp])
    #print(np.shape(freqs))
    f_f0 = f_weights(freqs, hp=hp)
    xi = SERs * f_f0
    
    # sample the prior on loss angles
    theta0 = np.meshgrid(hp['phi_sub'], hp['phi_coat'], hp['phi_clamp'], indexing='ij')
    thetas = np.vstack([theta.ravel() for theta in theta0])

    try:
        # evaluate utility function at experimental design (xi)
        cost = -U_FIG(xi, thetas, hp=hp)
    except:
        return np.inf

    global cost_min
    global x_best
    if cost < cost_min:
        cost_min = cost
        x_best = x
        print('new optimum ' + str(cost_min) + ' at ' + str(x_best))
        #print(x)
    else:
        print('optimum remains ' + str(cost_min) + ' at ' + str(x_best))
        #print(str(cost_min) + ' at ' + str(x)) 
    return cost #np.log(cost)



def _compute_cost(x, *args): #params, model=None, eng=None):
    #print(args)
    argmap = args[0]
    #print(argmap)
    for i in range(len(argmap)):
        arg = argmap[i]
        #print(arg)
        #print(args[i])
        if arg == 'model':
            model = args[i]
        elif arg == 'eng':
            eng = args[i]
        elif arg == 'params':
            params = dict(zip(args[i], x))
        elif arg == 'phi_clamp':
            phi_clamp = args[i]
        elif arg == 'phi_sub':
            phi_sub = args[i]
        elif arg == 'phi_coat':
            phi_coat = args[i]

    px, pparams = parseParams(params)

    global cost_min
    global x_best

    try:
        freqs, SEsub, SEcoat, SEclamp = eng.run_experiment(
            px, pparams, model, nargout=4
        )
    except:
        print('failed to run model at ')
        print(x)
        print('optimum remains ' + str(cost_min) + ' at ' + str(x_best))
        return np.inf

    SEtot = np.array(SEsub) + np.array(SEcoat) + np.array(SEclamp)
    SERcoat = np.array(SEcoat) / SEtot
    SERclamp = np.array(SEclamp) / SEtot
    SERsub = np.array(SEsub) / SEtot
    
    phi_tot = phi_sub * SERsub + phi_clamp * SERclamp + phi_coat * SERcoat
    dPhi_coat = phi_coat * SERcoat / phi_tot
    dPhi_clamp = phi_clamp * SERclamp / phi_tot

    
    cost = -np.log(
        np.sum(dPhi_coat / np.abs(np.array(freqs))) /
        np.var(np.log(dPhi_clamp))
    )
    
    if cost < cost_min:
        cost_min = cost
        x_best = x
        print('new optimum ' + str(cost_min) + ' at ' + str(x_best))
        #print(x)
    else:
        print('optimum remains ' + str(cost_min) + ' at ' + str(x_best))
        #print(str(cost_min) + ' at ' + str(x))

    return cost


    

# put parameters for COMSOL model into proper format
def parseParams(params):
    pparsed = []
    xparsed = []
    for p in params.keys():
        if p == 'thickness':
            pparsed.append(str(p))
            xparsed.append(str(params[p]) + "[um]")
        elif p == 'coating':
            pparsed.append(str(p))
            xparsed.append(str(params[p]) + "[nm]")
        elif p in ['h_short', 'h_middle', 'h_long', 'l_leg', 'l_box']:
            pparsed.append(str(p))
            xparsed.append(str(params[p]) + "[mm]")
        else:
            pparsed.append(str(p))
            xparsed.append(str(params[p]) + "[mm]")
    return xparsed, pparsed

def generateData(hp):

    params = hp['params0']

    # x0 is true value of parameters in an ordered list
    x0 = []
    p0 = []
    xnom = []
    for p in params.keys():
        p0 = np.append(p0, p)
        xnom = np.append(xnom, params[p])
        if p in hp['p_geom']:
            x0 = np.append(x0, params[p] * (1 + randn() * hp['sig_geom']))
        elif p in hp['p_loss']:
            x0 = np.append(x0, params[p] + randn() * hp['sig_loss'])

    #y0 = -np.inf
    #while y0 == -np.inf:
    y0 = lnpost(x0, params=p0, fiducial=True, hp=hp)
    
    
    data = np.zeros(np.shape(y0))
    #print(y0)
    #print(np.shape(y0))
    # iterate over eigenmodes
    for i in range(len(data)):
        # iterate over data type (frequency or Q)
        for j in range(len(data[i])):
            if i == 0:
                data[i, j] = y0[i, j] + randn() * hp['sig_f']
                #print(data[i,j])
            elif i == 1:
                data[i, j] = y0[i, j] * (1 + hp['sig_Q'])
                #print(data[i,j])

    nwalkers = hp['nwalkers']
    ndim = hp['ndim']

    theta0 = np.array(
        [[xnom[i] * (1 + randn() * .01) for i in range(ndim)]
         for ii in range(nwalkers)]
    )

    return x0, p0, y0, data, theta0


def lnprior(params, hp=None):

    #print(params)
    params0 = hp['params0']
    #print(params0)
    prior = float(0)

    #print(hp['sig_geom'])
    #print(hp['sig_loss'])

    for p in params.keys():
        if p in hp['p_geom']:
            if params[p] < 0:
                return -np.inf
            prior += np.log(norm.pdf(
                params[p], params0[p], params0[p] * hp['sig_geom']))
        elif p in hp['p_loss']:
            if params[p] > 0:
                return -np.inf
            prior += np.log(norm.pdf(
                params[p], params0[p], np.abs(hp['sig_loss'])))
    
    return prior

def lnpost(
        x,
        params=None, fiducial=False, hp=None, data=None
):

    x = np.ndarray.flatten(x)

    prior = lnprior(dict(zip(params, x)), hp=hp)
    if prior == -np.inf:
        print('impossible prior at ')
        print(x)
        return -np.inf
    
    lnlike = float(0)

    try:
        #print(params)
        #print(x)
        #print(hp['model'])
        #print(hp['eng'])
        #print('running experiment')
        y = run_experiment(dict(zip(params, x)), model=hp['model'], eng=hp['eng'])
        #print(np.shape(y))
        #print(np.shape(data))
    except:
         print('failed to compute model at ')
         print(x)
         return -np.inf
        
    if fiducial:
        return y
        
    try:
        #print(lnlike)
        #print(data)
        for i in range(len(data[:,0])):
            for j in range(len(data[i,:])):
                if i == 0:
                    lnlike += np.log(norm.pdf(data[i,j], y[i,j], y[i,j] * hp['sig_f']))
                #print(lnlike)
                elif i == 1:
                    lnlike += np.log(norm.pdf(data[i,j], y[i,j], y[i,j] * hp['sig_Q']))
                #print(lnlike)
    except:
        print('bad statistical comparison at ')
        print(x)
        return -np.inf
        
    #lnlike += -0.5 * np.sum((ff - f_data)**2)
    #lnlike += -0.5 * np.sum((QQ - Q_data)**2)
    
    #print(lnlike)
    #print(prior)
    return lnlike + prior
