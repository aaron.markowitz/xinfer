function [freqs,substrate,coating,clamp] = run_experiment(x, params, model)
  
  model = update_model(x, params, model);

  if model.param.evaluate('h_base') <= model.param.evaluate('h_clamp')
    return
  elseif model.param.evaluate('mid_space') <= 0.0
    return
  elseif model.param.evaluate('edge_space') <= 0.0
    return
  end
  
  model.component('comp1').geom('geom1').run;
  model.component('comp1').geom('geom1').run('fin');

  model.component('comp1').mesh('mesh1').run;
  model.sol('sol1').runAll;

  freqs = mphglobal(model, 'freq');
  substrate = mphint2(model, {'solid.Ws'}, 'volume', 'selection', 'geom1_csol1_dom');
  coating = mphint2(model, {'solid.Ws'}, 'volume', 'selection', 'geom1_ext2_dom');
  clamp = mphint2(model, {'solid.Ws'}, 'volume', 'selection', [1 3]);
