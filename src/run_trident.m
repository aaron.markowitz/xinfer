function [freqs,substrate,coating,clamp] = run_trident(x, params, model)
  
  model = update_model(x, params, model);

  if model.param.evaluate('h_excess_base') < 0.0 %model.param.evaluate('h_clamp')
    return
  elseif model.param.evaluate('h_excess_coat') < 0.0
    return
  %elseif model.param.evaluate('mid_space') <= 0.0
  %  return
  %elseif model.param.evaluate('edge_space') <= 0.0
  %  return
  elseif model.param.evaluate('w_box') > model.param.evaluate('w_max')
    return
  %elseif model.param.evaluate('h_coat') >= model.param.evaluate('h_tot') - model.param.evaluate('h_clamp')
  %  return
  end
  
  model.component('comp1').geom('geom1').run;
  model.component('comp1').geom('geom1').run('fin');

  model.component('comp1').mesh('mesh1').run;
  model.sol('sol1').runAll;

  freqs = mphglobal(model, 'freq');
  substrate = mphint2(model, {'solid.Ws'}, 'volume', 'selection', 'geom1_csol1_dom');
  coating = mphint2(model, {'solid.Ws'}, 'volume', 'selection', 'geom1_ext2_dom');
  clamp = mphint2(model, {'solid.Ws'}, 'volume', 'selection', 'geom1_csel1_dom');
