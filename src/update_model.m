
function out = update_model(x, params, model)

for i = 1:length(x)
	  %disp(params(i))
	  %disp(x(i))
	  model.param.set(params(i), x(i));
end

out = model;
